
import random
import json
import os


latest_account_file_path = "latest_account.txt"
accounts_file_path = "accounts.json"



def write_accounts_to_file(accounts):
    with open(accounts_file_path, "w") as f:
        f.write(json.dumps(accounts))



def write_latest_account_number_to_file(latest_account_number):
    with open(latest_account_file_path, "w") as f:
        f.write(latest_account_number)


def get_latest_account_number():
    latest_account_number = "1000"

    if os.path.isfile(latest_account_file_path):
        with open(latest_account_file_path, "r") as f:
            temp = f.read()
            if temp:
                latest_account_number = str(int(temp) + 1)

    write_latest_account_number_to_file(latest_account_number)
    return latest_account_number


def user_login_options():
    while True:
        print("""
        1.Sign-up
        2.Login
        3.Help
        4.Exit
            """)

        login_ans = input("Please enter option number\n")
        if login_ans == "1" or login_ans == "2" or login_ans == "3" or login_ans == "4":
            break

        print("Sorry, unsupported command.")

    return login_ans


def access_menu(accounts):
    while True:
        login_ans = user_login_options()
        if login_ans == "1":
            sign_up(accounts)
            # still in the making
            # need to define exit
        elif login_ans == "2":
            account_id, account = login()
            if account_id and account:
                return account_id, account
        elif login_ans == "3":
            print("We will call you right away!")
            # still in the making
        elif login_ans == "4": # exit
            print("Bye Bye")
            break
            # still in the making
        else:
            print("Sorry, this is not a valid option.")
        # there is no "break" because we want the menu to always pop up.

    return None, None


def login():
    user_name = input("Hey! Please enter your name.")
    user_num = input("Please enter your bank account number\n")
    # need to define only numbers
    password = input("Please enter you password\n")
    # need to build & run authentication system which will look the username in the database
    if user_num and password in atm_details.txt:
        print(f"Welcome back {user_name}! your account number is {user_num}.")
        return user_num , accounts[user_num] # login success


        #atm_details.get_command_from_user() # there is something wrong in the command
    else:
        print("Sorry, we could not find you in our system.")
        return False, False
    # print is the user does not exists in the database - done.



def sign_up(accounts):
    account_number = get_latest_account_number()
    acc_name = input("Please enter your name...")
    new_pass = input("Please choose your password.")
    print(f"Welcome {acc_name}! this is your new account number: {account_number} and this is your password {new_pass}.")

    # todo: change the way passwords are being saved in a file to make them more secure. save the passwords hashed (like md5, sha1 .. ) to the file

    accounts[account_number] = {
        "account_name": acc_name,
        "balance": "1000",
        "password": new_pass
    }

    write_accounts_to_file(accounts)

