import os
import json
import login_menu

"""
this is explanation ATM

"""

latest_account_file_path = "latest_account.txt"
accounts_file_path = "accounts.json"
accounts = {}

# current logged in account
account_id = ""
account = {}

if os.path.exists(accounts_file_path):
    with open(accounts_file_path, 'r') as k:
        data = k.read()
        accounts = json.loads(data)


def update_db_json():
    global accounts
    global account_id
    global account

    accounts[account_id] = account
    with open(accounts_file_path, "w") as f:
        buf = json.dumps(accounts_file_path)
        f.write(buf)
#nooooooooooooooooooooooooooo

# write a ATM program. it will have a default account balance (10004)

## everything should be stored and loaded from ATM_details.json

# ATM_details = {
#     "1234": {
#         "balance": 45453,
#         "name": "lior",
#     }
# }
#

# menu
# 1. sign up to ATM
# 2. Log in
#   |
#   V

# homework details:
#    1. add an option for a new user to register, every user starts his account with 1000 balance
#    2. when user press 2 to login, ask for the account number. print if the account number doens't exists in your json, and exit
#   if exists:
#    3. When the menu pops up, print "Hello" + account name



# write a menu for the user (UI) , that will have the following options:
#   1. check balance
#   2. deposit
#   3. withdraw
#   0. exit

# each option of the menu will be written in a function

# command_number_to_command = {
#     "1": "check balance",
#
# }

# how to use json lib from last class
"""
import json


# with open("atm_details.txt", "w") as f:
#     atm_as_json = json.dumps(ATM_details)
#     f.write(atm_as_json)

# with open("atm_details.txt", "r") as f:
#     atm_dict = json.loads(f.read())


print()

{
    "account_number": "sdfsd",
    "account_name": "lior",
    "balance": "353453"


}
"""
# end of how to use json lib from last class


def get_command_from_user():
    while True:
        print("""
1.Check my balance.
2.Deposit.
3.Withdraw.
4.Exit/Quit
            """)

        ans = input("Please enter option number\n")
        if ans != "1" and ans != "2" and ans != "3" and ans != "4":
            print("Sorry, unsupported command.")
        else:
            break

    return ans


def deposit():
    global balance
    deposit_amount = input("How much you wish to deposit?")
    ans2 = input("Are you sure you want to deposit amount of " + str(deposit_amount) + "?")
    if ans2 == "yes":
        try:
            deposit_amount = int(deposit_amount)
        except Exception as e:
            print("invalid amount")
            return "Your balance is currently:" + deposit_amount

        balance = balance + deposit_amount
        update_db_json()


    elif ans2 == "no":
        print("Exiting deposit.")
    else:
        print("\n Sorry! Not Valid Choice. Please Try again")



def withdraw():
    global balance
    ans3 = input("\n Do you wish to withdraw?")
    if ans3 == "yes":
        withdraw_amount = input("OK. How much do you wish to withdraw?")
        try:
            withdraw_amount = int(withdraw_amount)
        except Exception as e:
            print("invalid amount")
            return

        ans4 = input("Are you sure you want to withdraw" + str(withdraw_amount) + "BTC?")
        if ans4 == "yes":
            if withdraw_amount < balance:
                balance = balance - withdraw_amount
                update_db_json()
            else:
                print("Sorry, Your balance is limited.")
        elif ans4 == "no":
            print("Exiting withdraw.")
        else:
            print("\n Sorry! Not Valid Choice. Please Try again")


def atm_menu():
    while True:
        ans = get_command_from_user()
        if ans == "1":
            print("\n your balance is currently: " + str(balance))
        elif ans == "2":
            deposit()
        elif ans == "3":
            withdraw()
        elif ans == "4":
            print("\n We're sorry to see you go!")
            break

def main_menu():
    global account_id
    global account
    global accounts
    account_id, account = login_menu.access_menu(accounts)
    if not account_id:
        return



# __name__ is a special variable definded by python itself when running a program. it will be '__main__' if the file running was the initial running point
# or the name of the file , if it was imported by another file
if __name__ == '__main__':

    main_menu()

