
# main memmory - that's the memory that any program will use to save its things objects, variables ... this usually speak about RAM memory but could also be CPU cache, registers etc. which is a volatile נדיף
# secondry memory -  that's the persistent memory (non - voltaile) which will be used to save things even after the program died or the computer lost power. for example: hard disk , meachinic or SSD, disk on key, CDROM, floppy disk





# with open("my_diar1y.txt", "a") as a:
#     a.write("Adding first lines to my_diary file\n")
#     a.write("Adding second line to my_diary file\n")

# if you call read() with no paramters it will read the whole file into the variable.  however it can get a paramters of type int that will tell it how many bytes to read
with open("my_diar1y.txt", "r") as a:
    #content = a.read(4)
    content = a.readlines()
    # content = a.read()
    print()









#FILES. 2 ways to open a file.


# First way:
# open is the function name. first parameter is the file name (or path) with the file extension , second paramter is the mode (w - write, r -read, a - append , wb - write binary, rb - read binary)
# open returns a handle to the file which is saved in a variable of your choice
# a handle is a special variable that the OS gives to a program which is used to handle files (open them, read the, do whatever u want with them)

# a = open("file.txt", "w")
# a.write("stam text")  # call write() on the handle in order to write things in the file that handle is pointing to
# a.close() # when you finish doing things with the file handle, you have to (!!!) close it !!!



#Second way:
# using with open() as some_file:
# whatever you do with the file handle inside the block will be saved, once you exit the block the file handle will be automatically closed by python



#modes
# w- write . if the file for write existed before, it will be overriden (!!!) so watch to not delete an existing file data by chance. if the file didn't exist, python will create it
# a - append. if the file exisats, this opens the file and putting the cursor at the end of the file by default, any write() functions will be written to the end of the file. if the file doens't exists it just creates it and writing to it
# r - read - if the file doesn't exists - you will get an error. if the file exists, you can read from it, with functions like read()



# with open("fileaa.txt", "w") as a:
#     a.write("aaa")
#     a.write("bb")









#
#
# #or the prefferd way, using 'with' keyword which automaticlly closes the scope for you so you need to close your handles.
# with open("file.txt", "w") as a:
#     file.write("stam text2\n")
#
# #w is used for opening the file for writing. r is for reading. There are more options
#
# # popular modules - os
#
# #print all the files in current directory
# for i in os.listdir(os.getcwd()):
#     print(i)
#
