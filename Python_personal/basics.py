import os
import time
import math

#print math.log(16, 2)


#int = 5
#float = 4.3
#string = 'fgdg!@djfngjk455'
#boolean = True or False

# +
# -
# /
# *
# **


# ttt  = num1 * num2
# username = "dor"
# password = "1234"
#
# x = password * 4



num = 6
num = str(num)
print("your number is " + str(num))
print("jfsdjdsfg")



username = input("enter your username\n")




#
#print "your username is " + username


#Unlike other languages, python doesn't need you to declare varibale types (e.g. int, double, string...)
#In order to declare a variable in python, you just give it a name and assign something to it. For example:
i = 4
#we declared a variable named i and assign 4 to it. if we look at the type of it, it will say 'int'
# print type(i)
#in order to convert varibles to other types, we cant cast them with brackets :

i = str(i)

line = "the number is " + i
# print ("the number is " + i)
# print type(i)

# now i is a string and can be used with string manipulation


# what is the difference between i+x and str(i) + str(x) ?
i = 5
x = 4
print((i+x))

i = str(i)
x = str(x)
print((i + x))
# when they are interpted as strings, it appear the result is 54, but actually is just 2 chars combined together, here's another example
print((i + " some_text " + x))
# so as you can see, python is really easy when changing varibles types
#let's see what else can we do:
print(('a' * 50))
#when using string, we need to put quotes around them , doesn't matter if " or ' . With that being said, usually ' is for single chars while " is for string
# + - * /    - are the normal math operations
# ** is power of
print((2**3))
# % is the# modulu sign. it is used to calculate the remainder after division:
print((8%6)) # = 2. Divding 8/6 , leaves 2





#I give you a number that represent seconds. Calculate how many minutes are .
seconds = 90
print((str(seconds) + " seconds are 1 minute and 20 seconds"))
#notice how i case seconds with str() , so I can use it as a string


#this is a function declartion:
def calc_minutes(seconds_num):
    minutes = seconds_num / 60
    seconds = seconds_num % 60
    print(("In " + str(seconds_num) + " there are " + str(minutes) + " minutes and " + str(seconds) + " seconds"))


#this is how we call a function. In python you can declare a function anywhere in the code but make sure you call it only after you declare it !
calc_minutes(400)


#AS you saw earlier, you can use change the value of a varible, even from int to string without a problem, unline languges like c and Java
i = "now I'm a string"
print (i)
#Another way to use varibles in a string are placeholders. You use them like this
print(("%s is the time right now" % (time.time())))
# %is used as a placeholder, when you finished with your string, you put another % then put your varibles
print(("%d + %d is %d" % (5,2,7)))
# %d is used for decimals, %s is used for strings, %f is float
print(("%f" % (5)))

#\n is line space

print ('\n\n')


# Python data stracture:
#LISTS:
list_a = [1, 3, 4]
# string is also a list
a = "Nice Dog"
print((a[2])) #incding
#lists are zero based - we start to count from 0. so a[2] is the third char in the string == c
#you can combine a list with different types:
list_b = ['a', 2, 5.0]
print (list_b)

# in order to add more to the list
list_b.append('c')
print (list_b)
#in order to pop the last added element from the list
list_b.pop()
print (list_b)
# you can also remove an elment with its index
popped = list_b.pop(0)
print (list_b)

#DICTIONARIES:
#they are hash maps , very useful if want to arrange types to values. The idea behind is used in many lanagues, and also in JSON
dict_a = {1: 'a', 2: 'b', 3: 'c'}
#Let's say we want to print the third letter in the alphabet
print((dict_a[3]))
#square brackets are used to print the value behind the elemnt in the brackets
#if the element doesn't exist , it will cause an KeyError excpetion
#print dict_a[6]



#SETS - maintain only one instance of each object
s = {1,3,1,1,1,2}
print (s) # 1 will be saved only once
#we can also convert list to set
ss = [1,3,1,1,2]
ss  = set(ss)
print(ss)


#If STATMENTS
# if is used to check if something is true or not
if 2+1 == 3:
    print("y")
#== is used for equlaity. != is used to check if not equal. Also have or & and
if 2 != 3:
    print("y")
#booleans True and False can be used in varios ways
if True:
    print("true")
t = True
if t:
    print("true")
if not t:
    print("not true")


#if we want to check several contions we use elif and else
some_num = 10
if some_num == 9:
    print("9")
elif some_num == 5:
    print("5")
elif some_num == 6:
    print("5")
elif some_num == 7:
    print("5")
elif some_num == 8:
    print("5")
else:
    print("not found")



#LOOPS:
#there are 2 kinds of loops in python, for loops and while loops
i = 1
while i < 5:
    print(i)
    i = i +1

print("\n\n")
#be careful with while loops, if you won't have a break contions you'll end up in an infinite loop!
#using while loop we cant break or move to the next iteration if a statment occurs
# can you guess what digits will be printed out here?
i = 1
while (i< 5):
    print(i)
    if i == 3:
        break
    i = i +1


print("\n\n")
#For loops. Are used to iterate over a list/dictionary or any other itertable object
for i in list_b:
    print(i)

print("\n\n")

some_str = "nice"
for i in some_str:
    print(i)
#continue is used to skip this iteration and go to the next one. what will be printed out here??
for i in range(8):
    if i == 2 or i == 4:
        continue
    print(i)

print("\n\n")

#notice that in range we start from 0 but end in index-1. we can also do assign a start and end indexes and declare skips
for i in range(1, 10, 2):
    print(i)

print("\n\n")

#ths same idea can be done in lists
new_list = [1,2,3,4,5,6,7]
print(new_list[1:5]) # will print 2 to 5
print(new_list[::2]) #will print the whole list but skipping of 2
print(new_list[-1]) # will print the last elemnt
print(new_list[::-1]) #will print the whole list backwards , since it's skipping of -1


#another useful keyword is 'pass' , as it hints, it doesn't do anything. Can be used if you like to implment some funciton later and just want to declare it for now
def will_do_later():
    pass


#write a simple program that takes the current date from a user and prints how much days have passed since the year beginning
# def calc_days_from_year_start():
#     days_to_months = {1: 31, 2: 28, 3:31}
#     total = 0
#     month = int(raw_input("What month is it ?(enter with a number)\n"))
#     day = int(raw_input("What day is it?\n"))
#     for i in range(1, month):
#         # i = i+1
#         total = total + days_to_months[i]
#     total = total + int(day)
#     print str(total) + " days have passed since the beginning of the year"


# calc_days_from_year_start()



# try - catch
print("\n\n")

new_dict = {1:'a', 2:'b'}
try:
    print(new_dict[4]) #key doesn't exists
except KeyError:
    print("error key doesn't exist")

print("\n\n")

#passing arguments to function and defaults. Default can be used when you want to let the user of the fucntion with a varible number of varibles

def some_func(x,y,z=None):
    print("x is " + str(x))
    print("y is " + str(y))
    if z:
        print("z is " + str(z))


some_func(12, 4)
#and now with another paramter
print("\n\n")

some_func(12, 4, 5)

#return data from function. You can also return more than 1 variable, unlike other languages

def add(x,y):
    return x+y

result = add(2,3)
print(result)

def return_2():
    return 3,4

result1 = return_2() # the return object is tuple, you access elemnts like in list
print(result1[0])
print(result1[1])


#FILES. 2 ways to open a file.
a = open("file.txt", "w")
a.write("stam text")
a.close()
#or the prefferd way, using 'with' keyword which automaticlly closes the scope for you so you need to close your handles.
with open("file.txt", "w") as file:
    file.write("stam text2\n")

#w is used for opening the file for writing. r is for reading. There are more options

# popular modules - os

#print all the files in current directory
for i in os.listdir(os.getcwd()):
    print(i)

#classes and OOP - to be added