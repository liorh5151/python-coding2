# you can write to stdout for debugging purposes, e.g.
# print("this is a debug message")


import pandas as pd


def solution(S, C):
    # write your code in Python 3.6
    values = []
    rows = S.split("\n")
    header = rows[0]
    columns_names = header.split(",")
    for idx,current_column in enumerate(columns_names):
        if current_column == C:
            break
    # print('idx: ', idx)

    for current_row in rows[1:]:
        values.append(int(current_row.split(",")[idx]))
    return max(values)


if __name__ == '__main__':
    my_S = 'id, name,age, act,room,dep.\n1 ,jack,68,T,13,8\n17,Betty, 28,F.15,7'
    # res = solution(my_S, "age")
    my_S = 'area,land\n3722,CN\n6612,RU\n3855,CA\n3797,USA'
    res = solution(my_S, "area")

    print(res)