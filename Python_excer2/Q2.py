def solution(A, D):
    # write your code in Python 3.6

    total_fee_per_year = 5 * 12
    num_discount = 0
    sum = 0
    for elem in A:
        sum = sum + elem

    months = dict()
    tarnsactions = dict()
    for date, transaction in zip(D, A):
        if transaction < 0:
            month = date.split('-')[1]
            if month in months.keys():
                months[month] += 1
                tarnsactions[month] += abs(transaction)
            else:
                tarnsactions[month] = abs(transaction)
                months[month] = 1

    for month_name in months.keys():
        if months[month_name] >= 3 and tarnsactions[month_name] >= 100:
            num_discount += 1

    total = sum - total_fee_per_year + num_discount * 5
    return total


if __name__ == '__main__':
    # my_A = [100, 100, 100, -10]
    # my_D = ["2020-12-31", "2020-12-22", "2020-12-03", "2020-12-29"]
    # my_A = [180, -50, -25, -25]
    # my_D = ["2020-01-01", "2020-01-01", "2020-01-01", "2020-01-31"]
    # my_A = [1, -1, 0, -105,1]
    # my_D = ["2020-12-31", "2020-04-04", "2020-04-04", "2020-04-14", "2020-07-12"]
    my_A = [100, 100, -10, -20, -30]
    my_D = ["2020-01-01", "2020-02-01", "2020-02-11", "2020-02-05", "2020-02-08"]
    solution(my_A, my_D)
