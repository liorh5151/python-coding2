def is_all_middle_lane_free(seats):
    for seat in seats:
        if seat != 'f':
            return False
    return True


def is_more_2_middle_lane_free(seats):
    counter = 0
    for seat in seats:
        if seat == 'f':
            counter += 1

    if counter == 2 or counter == 3:
        return True

    return False


def side_lane(seats):
    combinations = 0
    counter = 0
    for i, seat in enumerate(seats):
        if seat == 'f':
            counter += 1

    if counter == 2:
        combinations = 1
    elif counter == 3:
        combinations = 2

    return combinations


def solution(N, S):
    # write your code in Python 3.6
    # free
    # reserved
    init_row = ['f'] * 10
    all_rows = []

    for i in range(N):
        all_rows.append(init_row.copy())

    taken_array = S.split(' ')

    for seat_taken in taken_array:
        column = ord(seat_taken[1]) - ord('A')
        row = ord(seat_taken[0]) - ord('0') - 1
        all_rows[row][column] = 'R'

    number_of_possibilities = 0
    # first approach is to check all center lane (D-G)
    for idx, row in enumerate(all_rows):
        if is_all_middle_lane_free(all_rows[idx][3:7]):
            number_of_possibilities += 1
        elif is_more_2_middle_lane_free(all_rows[idx][3:7]):
            side_lane_combinations_left = side_lane(all_rows[idx][:3])
            if side_lane_combinations_left == 0:
                side_lane_combinations_right = side_lane(all_rows[idx][7:])
                if side_lane_combinations_right:
                    number_of_possibilities += 1
            else:
                number_of_possibilities += 1
        else:
            pos_left = side_lane(all_rows[idx][:3])
            pos_right = side_lane(all_rows[idx][7:])
            if pos_left and pos_right:
                number_of_possibilities += 1

    return number_of_possibilities


if __name__ == '__main__':
    solution(N=2, S="1A 2F 1C")
