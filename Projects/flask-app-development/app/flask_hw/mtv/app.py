from flask import Flask
from flask import render_template
import logging

app = Flask(__name__, template_folder='temp')

logging.basicConfig(filename='app.log', filemode='w', level=logging.DEBUG)


@app.route('/')
def index():
    return render_template('index.html')


if __name__ == "__main__":
    app.run(host = '0.0.0.0', port = 8080, debug = True)