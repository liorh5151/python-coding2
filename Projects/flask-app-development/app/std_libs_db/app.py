from flask import Flask
import logging

# Saving output of the application into a file (log files saves the app behavior).
# All operating system log files usually in Linux stores in /var/log.
# The logs won't be saved unless we define a file to save it.

 
app = Flask(__name__)

# How to define a log file (and not just a warning):
logging.basicConfig(filename='app.log', filemode='w', level=logging.DEBUG)

@app.route('/')
def index():
    logging.debug('The app has started')
    return "Hello from flask"


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080, debug=True)