click==8.0.4
colorama==0.4.4
commonmark==0.9.1
Flask==2.0.3
itsdangerous==2.1.0
Jinja2==3.0.3
MarkupSafe==2.1.0
Pygments==2.11.2
rich==11.2.0
Werkzeug==2.0.3
