
# THESE ARE EXAMPLES OF 'MODULAR STRUCTURE'. 
 
 
 # MVC
  - Model View Controller.
 Controller is the tool/file where we define the access to out application to places.
 View is a collection of files/dir that allowes us to view the files content UI 
 Model is the thing that contains the functions/classes for our application
 

# MTV
Instead of MVC we have something called MTV which is based on templates
 - Model Template View
The idea is the same idea between thr two models.
View is responsible for creating paths to out application
Model is the same (functions/classes for our application)
Template is a template that allowes us to reach some data (UI)

# MTV structure example:
#MTV is giving us structure for the following:
                MTV Structure
               /     |      \
          Views  Templates  Models
views --> The app's paths. Views knows to access the 'template' for needed data/variables.
Templates --> Where we store our UI under settings and arguments.
Models --> We're using it for data structures models for storing data. We'll define classes/function etc to a variable to provide easier memory access/storing.
The view file will also take the data from the 'Models' file too.
All the data from the 'views' file is being transfered to the web applicastion database, example below.

# MTV structure physical example:
                                --------
        |-- Views --------- ---------  |
MTV --> |                  |        |  |
        |-- Templates  <----        |  | ----> database
        |                           |  |
        |-- Models       <-----------  |   
                                --------


