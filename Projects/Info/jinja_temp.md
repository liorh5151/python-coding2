# 'Jinja'

We have something calles "Jinja templates".
The idea of 'Jinja' templates is that it allowes us to dynamically update values inside the app's front-end.
In other words we can define vlaues in files like the 'views.py' or 'web_app.py' file and the UI will run by these vlaues.

#############################################

# Usually when we will write Jinja we will write loops or conditions --> examples balow.

# Conditions:
{% if {condition} %}
        {action}
    {% elif {condition} %}
        {action}
    {% else {condition} %}
        {action}
    {% endif %}

#For example:
{% if title %}
            <title>{{title}} - Resume</title>
        {% else %}
            <title> Welcome </title>
        {% endif %}
#Here we basically take the back-end to the fromt-end.

# Loops:
{% for {variable} in {variables} %}
        {action}
    {% endfor %}

{% while {condition} %}
        {action}
    {% endwhile %}

#For example:
{% for var in vars %}
        print "Hey"
    {% endfor %}


#############################################

# Another ability of Jinja:
Jinja template can take a huge html file, like the 'index.html' file and cut it into sections so we can have a convenient way to work on these files.
# How?
We create a 'base.html' file which usually containes the header (the head part in the html template), the footer, and also the libraries links.
Eventually, all we have in the actual 'index.html' is the body section, ehrn the rest of the file is in the 'base.html' file.
  
#############################################

# Inheritance (like importing):
In Jinja we're importing/connecting one file to another, by object inheritance from one file to another.
In this action we will save all the handeling with configuration files etc.
How do we do that?
We will go to the 'index.html' and define a new Jinja teg calles 'extends'.
# For example:
{% extends 'base.html'%}
{% block body_content%}
# We can find inside the 'index.html' and 'base.html' files, 
We saved parts of the data in these two different files do we had to import them.

#############################################

# Side notes:
Jinja has the ability to work inside the files and change them.
Jinja is not TAB sensitive.

# Another part of our Jinja template will be stored in a file called 'signin.html'.