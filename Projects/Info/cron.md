
# This file contains inforamtion about Schedular and crom-jobs in Linux.

#######################################################################

# Schedular:
In Linux we have comthing called 'Schedular' which Schedule future constant repeated missions/processes (cron-jobs).
For example: Run a specific script every year to renew SSL certificate.

#######################################################################

# Crontab (https://crontab.tech/):
'Crontab' is a Schedular tool in order to Schedule sonstant missions/processes.
The crontab has 6 fiels (שדות) which contains the following data in order to sxecute the missions:

|-----------------|
|  * / 5 * * * *  |
|-----------------|

- Minute
- Hour
- Day (day in the month)
- Month
- Day (day in the week)

For example:
Every 10 Minutes --> '*/10 * * * *'

#######################################################################

# Rebooting:
The system won't save our script running every time we'll run the machine, so we will add it to the cron-jobs,
So the script will run automatically every time the machine is cooting up.
But... see below.

#######################################################################

# Cron-jobs downside:
We can't relay on schedular to run our script from the reason that the schedular is a service,
And service can always be eliminated, or the schedular itself.
We need to replace this option to make the script constantly running (more info below).

#######################################################################

# What can we do instead?
We will insert out script to run inside the system's boot-time tasks.
We can do it with the command 'systemctl'.

Inside the operating system we have Linux processes (we can see then with the command 'ps -elf').
We also have "special" processes, which aren't running by the user, they are running by the system.
In processes we have:
    - PID --> Process ID
    - PPID --> Parent Process ID
In Linux we have one process which is the PPID (Parent Process ID) of them all and its name is 'systemd' (used to be by the name init.d)
The PPID 'systemd' is responsible to run all the processes in out Linux machine.

#######################################################################

# Daemons:
In Linux the processes are calles 'Daemons' (in Windoes it's services).
'Daemons' are services which systemd 'responsible' to run.
We will add a daemon so the script will be running with boot time.
(if we're adding pipenv to the daemons we can also enter a specific application - by that we can run a virtual environmant as a boot service).
In other words - we'kk use 'systemd' as a process.

#######################################################################

# Run levels
'systemd' is running the 'daemons' (services) on Linux.
These processes are running by something called 'Run-level'.
'Run-levels ' is the steps for the running of the operating system, in Linux we have 7 run-levels and goes (0,1,2,3,4,5,6).
Every run level has an action specific for that level (0 and 6 are special]).
- Runlevel 0 (special) --> general notice for the all services to disconnect and die ('halt' command).
- Runlevel 1 --> single user mode.
- Runlevel 2 --> multi-user mode without networking.
- Runlevel 3 --> multi-user mode with networking.
- Runlevel 4 --> user-definable.
- Runlevel 5 --> multi-user mode with networking
- Runlevel 6 (special) --> general notice for the all services to perform reboot/reset ('reboot' command).

But all these 'runlevels' are useless because instead of run levels in systemd we have somthig called 'targets'.
'Targets' are sorted by these runlevels.
In order to see the target's list we will type the command 'systemctl -t target'. 

- The operating systems Debian & Ubuntu stopped using runlevels 3-5 (we have runlevels of 0 1 2 6).
- The operating systems Red hat stopped using runlevel 4 (we have runlevels of 0 1 2 3 5 6).
Because 4 is 'custom run level', this action unable more customization for the operating system.

When we define services, our services are services which will operate and run by the instructions we give them (more info in the following section).
For example when we have to followint thing inside the service template -> WntedBy=multi.user.target --> (down below)
We tell the service to the service to run when reaches the multi-user level (runlevel 2 un Ubuntu).

#######################################################################

# How to create a process that 'systemd' knows to execute? (how to add a daemon)
We can't change anything without being a root so we will type the command --> 'sudo su'.
#cd /etc/systemd/system --> in this file we can see out boot services, we cam also see a file called 'sshd.service', which sows us a template of how a service should look.
#cp sshd.service webapp.service --> this file shows how a service should look like.
#systemctl enable webapp.service
desired output --> 'Created symlink /etc/systemd/system/multi-user.target.wants/webapp.service → /etc/systemd/system/webapp.service'.
#systemctl status webapp.service --> see the service's status
#sudo systemctl start webapp.service --> run the service
Our service refused to run, what can we do to fix it?
#systemctl daemon-reload --> 
#sudo systemctl restart webapp.service
#systemctl status webapp.servic
Our service refused to run, what can we do to fix it?
'netstat' is an old command meant for port listening by tcp/udp/port number/srial numebr.
#netstat -tupln --> to see if our application's port number (8000) is running on one of the computer's ports.
If we don't have 'netstat' in our system we can use 'ss' instead.

#######################################################################

# How a service should look like?
A service file usually splits into 3 sections: Unit, Service, Install.

Sections breakdown:
#######################
Unit --> unit that defines out application. Every unit has parts, for example timer, service, storage etc.
Unit will usually contain to following things:
* Parts of a unit (by what the unit should run): 
- Descriptions = Python based app that we created --> descriptions of the application.
- Documentation = --> we won't use it.
- After = network.target  --> the target says when out app needs to run (here it says that the app will run after there's a network connection).
- ConditionPathExists --> we won't use it.
#######################
Service --> 
Service will usually contain the running instrucations for the application.
The service has a few parameters:
- EnviromentFile=-/home/vagrant/ --> the file's environment.
- ExecStartPre=/usr/sbin/sshd -t --> a test before the service is running --> we won't use it.
- ExecStart=/usr/sbin/pipenv run python web_app.py --> what commands should we run in order to run our application. (the python later changes to gunicorn).
- ExecReload=/usr/sbin/sshd -t --> what we need to do every time when the application reload --> we won't use it.
- ExecReload=/bin/kill -HUP $MAINPID --> same as the last one. We cam assign a few 'ExecReload's --> we won't use it.
- KillMode=process --> in what situation we will want to kill our process.
- Restart=on-failure --> on what conditions the app should restart (if the app crashed, then do a restart).
- RestartPreventExitStatus=255 --> if the exit status of out app is 255 (failure) then restart --> we won't use it.
- Type=notify --> which notifications we will want to get --> we won't use it.
- RunTtimeDirectory=vagrant --> from which folder the app should run.
- RunTtimeDirectoryMode=0755 --> permissions to run the application --> we won't use it.
#######################
Install --> 
Install will usually contain to following things:
- WantedBy = multi-user.target --> run this service then reach the multi-user level (runlevel 2 un Ubuntu). (more info about run levels up this file).
- Alias = sshd.service --> we won't use it.

#######################################################################

# How can we run a service (daemon)?
After creating out service (daemon for tha application), we need something to run it new service (the app).
There's a tool which responsible for running the python services, this tool called 'green unicorn', or in other words 'gunicorn'.

What is a 'gunicorn'?
'gunicorn' is a Python library which responsible for running Python.

How can we do it?
Until now we used the following command:
#pipenv run python web_app:app
But now we can type the following command:
#pipenv run gunicorn -w 4 web_app.py --> 'w' is for number of processes we want to limit for our app, in this case we'll choose 4 processes.
Besides running the app, with gunicorn we're running the app in a safer mode, when out ip address isn't visible to all.
gunicorn can deal with 4 processes but the app will crash with 16 processes (example for many processes running).

Again, this is the old waym complicated.

#######################################################################

# Web server and its job.
There's a tool which solves the problem of port 8080 or 443, this tool called 'web server'.
The web server is responsicle of getting requests to port 8080 er 443 and redirect the traffic to the application.
With the web server, the app knows how to deal with the requests and respond accordingly and our data ins't exposed.

2 popular web servers are Ngine-x (spelled engine-x) and Apache.

#######################################################################

# The process of our application (web_app.py):
    - Fix and install the 'gunicorn'.
        - Run the application.
           - Connect everything to the web-server so the inforamtion can reach destination.

# How can we do that?
We will use somthing called 'reverse-proxy'.
'proxy' is usually when users reach the browser without it knowing who they are.
'reverse-proxy' is when webservers reach one another without them knowing who they are.
The commands:
#sudo apt install nginx
#cd /etc/nginx
In this file we will see the configuration files of Nginx.
There we'll see:
modules-aviailable --> modules that ready and available to use (python libs).
modules-enabled --> modules that are under use (python libs).
sites-available --> sites that are under use (websites).
sites-enabled --> sites that ready and available to use (websites).
These files contain 'soft-links' to what our operating system is using.
It's important to remember that Nginx might looks different in every Linux operating system.
Now we will wdit the default file.
#nano /etc/nginx/sites-available/default
There we can see settings for connection with servers, connections with SSL certificate, etc.
In the file we can find the following things:
- The user & the app's path.
- The file type fot the file that's making a connection with the application.
- Additional files location.
- URL's location so we can reach the app.
#systemctl enable nginx
#systemctl start nginx
After this action we'll be able to reach our web server in the URL: 10.0.0.35:80 (80 is the browser default port).

#######################################################################

# Running the application? 'Socket'.
After loading and starting the Nginx web server, how do we load the app?
Well, we will use somthing calles 'socket'.
'socket' is the combination of an IP address + port, by using them we'll be able to connect to the web server.

# How do we create a 'socket'?
We will need to define a socket in out app and also in our web-server.
The Python module 'gunicorn' is what will create the 'socket'.

# How to define socket on the application?
We will configure the 'gunicorn' for the socket on the app services file (which we previously talked about).
#nano /etc/systemd/system/webapp.services
In this file we will add a new configuration to an existing one in the Service 'section' (previously mentioned).
- ExecStart=/usr/sbin/pipenv run gunicron -w 4 --bind unix:/home/vagrant/app.sock web_app.py --> what commands should we run in order to run our application.
With this action, we're telling gunicorn to go to a file called 'app.sock' (which will create after this command) and bind it into the 'web_app' in 10.0.0.27:8080.
In other words, all the app's traffic will redirect to this file in a binary format, the process will be same for the ouput as well. 
After changing somthing in the system we will always hve to reset the daemon (the processes in Linux system).
#systemctl daemon-reload
#systemctl restart webapp
#systemctl status webapp
After these commands, the app shoud be running (active status).
Now we need to make the application communicate with the web server.
For the service file of the app we will add 'User' (vagrant) in the 'Service' section, same for group.
So we added these lines --> 
User=vagrant
Group=vagrant
Afterwards, we will restart the daemon again.
After that and the web server part, our app should be up and running.

# How to define socket on the web-server?
Let's move to the web server and edit the 'socket'.
#nano /etc/nginx/sites-enabled/default .
To test the configuration changes:
#sudo nginx -t
Desired output --> nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successfule
#systemctl restart nginx
#systemctl status nginx
Desired output --> {
     ● nginx.service - A high performance web server and a reverse proxy server
     Loaded: loaded (/lib/systemd/system/nginx.service; enabled; vendor preset: enabled)
     Active: active (running) since Sun 2022-03-06 12:49:41 UTC; 13s ago
       Docs: man:nginx(8)
    Process: 9354 ExecStartPre=/usr/sbin/nginx -t -q -g daemon on; master_process on; (code=exited, status=0/SUCCESS)
    Process: 9355 ExecStart=/usr/sbin/nginx -g daemon on; master_process on; (code=exited, status=0/SUCCESS)
   Main PID: 9356 (nginx)
      Tasks: 3 (limit: 2340)
     Memory: 3.2M
        CPU: 17ms
     CGroup: /system.slice/nginx.service
             ├─9356 nginx: master process /usr/sbin/nginx -g daemon on; master_process on;
             ├─9357 nginx: worker process
             └─9358 nginx: worker process

Mar 06 12:49:41 appdb systemd[1]: Starting A high performance web server and a reverse proxy server...
Mar 06 12:49:41 appdb systemd[1]: Started A high performance web server and a reverse proxy server. 
}
#sudo systemctl restart webapp
#sudo systemctl status webapp
#journalctl -xe --> (more information below)

If we will do the command 'netstat -tupln' we won't see the app's port number rnning on out ports, why?
The answer is because we defined 'socket', and the socket knows to run the app through it.

#######################################################################

# journalctl
'systemd' has bug detect and tracking system/tool better then init.d (the old name for 'systemd').
This system called 'journalctl', it's a tool which collects live logs and see every service.
We have to be root in order to execute journalctl commands.

#######################################################################

# Side note - how to run a specific application with 'pipenv'.
In this process we will use 'pipenv' ( with 'pipenv' we can install python libraries in a different environment).
The commands we will run:
#sudo apt-get install pipenv
#pipenv install -r requirements.txt
#pipenv run python web_app.py (we directed pipenv to run on a specific app)
