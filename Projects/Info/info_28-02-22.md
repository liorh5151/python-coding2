# THIS FILE IS MEANT FOR NOTES FOR THE 28/02/22 CLASS.

# Artifact.
Artifact is a wrapped product in a certain way in purpose of assimilation (הטמעה), in a new clean server.
In other words, Artifact is a wrapped product which is ready to be entered to the server for the first time, linka like a finished product.
We write the code and it's done, but in order to make the code work and execute it we eill have to wrap it in a certain way.
We have a few ways of wrapping code and it's different for every code/app.

##################################

# Code wrapping example:
If we have a desktop-based product (it needs & meant to be ona desktop),
We need a server and in order to push it into the server we need to wrap the file and convert it into a binary file, which is executable.
In this case we can use a tool which will convert the code into a binary one and make it executable.

# 'pyinstaller':
In Python we call it 'pyinstaller'.
'pyinstaller' is a module (library in puthon) which takes the code and convert it to an executable file.
'pyinstaller' meant primary for desktop applications and not for websites.

##################################

# If we want a desktop-based app or script which is supported by the operating system,
# we can wrap it in the following operating system's foramts: 
(These are formats for executable files)

# 'deb':
In Ubuntu or Dabian we call it 'deb'.
# 'rpm':
In Red-hat ot Fedora or CentOS or Rocky we call it 'rpm'.
# 'exe'/'msi':
In Windows we call it 'exe' or 'msi'.

##################################

# Another way of wrapping files.

The following options are ways of wrapping a product while using the minimum memory space (common).
 
'Tar', 'gz', 'gzip2', 'xz', 'lmxz'.
The 'tar' command in Linux will take a collection of files into a highly compressed archive file.
In other words the 'tar' command will shrink the files in order to save artifact memory.

##################################

# Artifact storage.
 
The easiest to deal with the artifact storage we will use storage system, in the storage system we will have the following options and more:
The common options ar:
- nfs 
- samba
- nexus
- artifactory

For out product we will use 'nexus'.

##################################
# Wrapping process:

This is how we will wrap out file and shrink it with 'tar'.
We will use the command 'tar cvzf' (command initials--> create, verbose, gzip, force) 

'tar' command initials breakdown:
- create --> create the tar (pressed archive file).
- verbose --> prints what's happening behind the scenes (all the processes).
- gzip --> highly compressed the file with 'zip'.
- force --> force all files (to solve permissions blocks).
(Accourding to the file '.gitignore' we can't add out database, so we'll delete it to prevent any blocks for out action).

The finall & full command will be: 'tar cvzf web_app.tar.gz'.
(In some cases we will see the extension 'tgz' or 'gz'.)

'tar' command breakdown:
tar cvzf {new file name}.gz/tgz {files to compress}
After we highly compressed the files, we need to assimilate (להטמיע) the product.

##################################
