# Explanation for this file is in the 'tinydb.md' file.

# In this file we will try to create a certain data structure for out db.
# Here we will use classes (like regular classes data structures in python).

# Importing tables structures from 'flask'.
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired
# Importing Data validators, this is an argument that will demend the user to enter validation/input.

# Here we will inherite a class called 'FlaskForm', following this action we are creating a form.
class UserForm(FlaskForm):
    # This is requiring the user to enter his user name.
    username = StringField('Username', validators = [DataRequired()])
    # Requiring the user to enter his email address.
    usermail = StringField('Email', validators = [DataRequired()])
    # This is the submit option.
    submit = SubmitField("Send")
    # The results for this form will be stored in the file called 'views.py'.

# This is one option to deal with the POST requests and put them inside a database.
"""    
# In order to insert out POST request data (the user's input) we will create a function.
def post(**kwargs): # '**args'/'**kwargs' is a function definitions in python is used to pass a keyworded, variable-length argument list.
    username = UserForm.username
    usermail = UserForm.usermail

"""