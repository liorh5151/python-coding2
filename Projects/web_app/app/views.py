# In this file we will create our app's paths.

import os
import sys
from app import db
from flask import request  # allowes us to send+recieve data in HTTP requests shape.

from app import app
# Because the 'views' file isn't familiar with the app itself.

from flask import render_template
from .models import UserForm

@app.route('/')
@app.route('/index.html')
@app.route('/index.php')
def index():
    user = {'username':'Alex', 'userlname':'schapelle'}
    return render_template('index.html', title='Home', user=user)

@app.route('/signin', methods=['GET', 'POST']) # Here we defined out app to get both 'GET' and 'POST' requests from the user.
def sighin():
    form = UserForm()
    # With the 'db.indert' command we will insert our data into the database.
    if request.method == 'POST':
        num = 0
        user = request.args.get('username')
        mail = request.args.get('usermail')
        db.insert({'user_' + str(num + 1):user, 'usermail':mail})
    return render_template('signin.html', title = 'Leave E-mail', form = form)
# Once our application has the user's input we can insert it into a database.

# In order to insert out POST request data (the user's input) we will create a function.
# This function will be stored in the file called 'models.py'.

# 'CSRF'
"""
# We have something called 'CSRF' which in other words is 'Cross-site scripting'.
Cross-site scripting is the ability of an attacker to send query requests (POST, GET) and by that reveal inforamtion.
# How can we avoid it?
We will creat a "secret file", a file which will prevent the query requests.
The easiest way is to create a 'config' file which will cotain pre-defined variables inside a configuration file
The rest of the explanation and the answer will be stored in a file called 'config.py'.
"""