# By creating the '__init__.py' file we define the app's structure to look like a class structure.
# This is the application's starting point.
# In this file we will import out application into the memory.

from flask import Flask
from .config import Config
from tinydb import TinyDB
# Here we are calling to 'tinydb'.

app = Flask(__name__)

app.config.from_object(Config)

# Here we eill define the database in a variable calles 'db', which will be used later.
db = TinyDB('web_app.db') # We can also define a path instead of a file name.

# Here we will be able define an object to enter to the database.
# 'tinydb' is similar to 'mongodb' which is working by KEY;VALUE pairs (object-based instead of data tables and files etc).

from app import views

