# The explanation for this file is in the 'views.py' file. Long story short, this file meant to prevent Cross-site scripting attack.

import os

# Here we will define a variable to store the 'secret' into the memory.
class Config():
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'You_w!ll-neverGUESS'
    # We're now defining the variable 'SECRET_KEY' to be defined as a environment variable else be defined by a string.
    
    
    
    
# HTTP request types
"""
POST/PUT --> When a client tries to send us data through the internet browser.
GET --> Get a file content by HTTPS and put it on the internat browser.
In the 'views' file we define how to client wants to get to the info but we did not defined what happens if the client wants to send us data.
How does it looks like? --> methods=['GET', 'POST'].
Additional information can be found in the 'views.py' file.
"""    
    
# Side notes:
"""
An environment variable will always by capital letters (uppercase).

    
    
"""