#!usr/bin/env python3
################
# While
# For
################


##################
#WHILE LOOPS
##################
"""
i = 0

while i < 10:
    print(i,end='\t') 
    i = i + 1
#we're getting a one line answer because of "\t" instead of "\n"
print(i)
"""

###############
#Break ; break the loop
#continue ; continue (start the loop all over again)

#while i < 10:
    #if i == 5:
        #print("Oh no! I reached 5")
        #break
    #The loop will stop after break
    #print(i,end='\t') 
    #i countdown will increase in 1 each loop.
    #i = i + 1
    #or (the same action)
    #i += 1
#for example:
#i = i + 1 --> i += 1
#i = i - 1 --> i -= 1
#i = i * 1 --> i *= 1
#i = i / 1 --> i /= 1
##i = i ** 1 --> i **= 1
#i = i // 1 --> i //= 1
#i = i % 1 --> i %= 1

"""
while i < 10:
    if i % 3 == 0:                                  
        print("{}".format(i))
        continue
    #"continue" brings me back to the start of the loop, which means that i will always be 0.
    print("i,end='\t'")
    i = i + 1
"""
""""
while i < 10:
    if i % 2 == 1:
        print("{}".format(i))
        continue
    print("i,end='\t'")
    i = i + 1
"""

"""
while i < 10:
    print(i)
    i = i + 1
else:
    print("i has reached its own potential")
"""

#################
#FOR LOOPS
#################


#lst = [9,8,7,6,5,4,3,2,1,0]

"""
for i in lst:
    if i % 2 == 0:
        print(i, "has NO reminder")
    elif i % 2 != 0:
        print(i, "has A reminder")
"""
"""
for i in range(len(lst)):
    print(i,lst[i])
    #The print command will print us the number "i" and the index of "i" in lst:
    #For example --> print(i,lst[i]) --> 9, 0 --> the number is 9 and the index of 9 in lst is o.
"""

"""
# Ranges --> range(Start point, End point, Jumping)
for i in range(0,30):
# The for loop will run from 0-29 (not including 30).
# If we won't put the 0 in the range, the loop will still run from 0-29 --> The starting point will always be 0 unless it was told differently.
# The range command is the same as "seq" command in Bash script.
    print(i)
"""
"""
while True:
    for i in range(len(lst)):
        if lst[i] % 2 == 1:
            print("fount irregular one: exiting")
            break
    break
"""

# BIU HW during class

# Task = predefine a and b print "hello world" if a is greater than b.
"""
a = 5
b = 2
if a > b:
    print("Hello World")
"""

# Task = predefine a and b print "hello world" if a is not equal to b.
"""
a = 5
b = 2
if a!=b:
    print("Hello World")
"""

# Task = predefine a and b, if a is greater than b print "yes!!" else print "no".
"""
a = 5
b = 2
if a > b:
    print("Yes!!")
else:
    print("no")
"""


# Task = predefine a and b, print "1" if a is equal to b, print "2" is a is greater than b, otherwise print "3".
"""
a = 5
b = 2
if a == b :
    print("1")
elif a > b:
    print("2")
else:
    print("3")
"""

# Task = predefine a, b, c and d print "hello" if a equal to b, and c is equal to d.
"""
a = 5
b = 2
c = 8
d = 6
if (a == b) and (c == d):
    print("Hello")
else:
    print("whoops")
"""

# Task = predefine a, b, c and d print "hello" if a equal to b, or c is equal to d.
"""
a = 5
b = 2
c = 8
d = 6
if (a == b) or (c == d):
    print("Hello")
else:
    print("whoops")
"""

# Task = print i while i is greater than o
"""
lst = [-1,0,1,2]
for i in lst:
    while i > 0:
        print(i)
        break
"""

# Task = print i while i is great than 0. if i is devided without remnent by 7 stop the loop.
"""
lst = [1,2,3,4,5,6,7]
for i in lst:
    while i > 0:
        print(i)
        break 
    if i % 7 == 0:
        break   
"""

# Task = loop through the items in the num list
"""
num = [0,1,2,3,4,5,6,7,8,9,10]
for i in range(1,11):
    print(i)
"""

# Task = loop through the items in the num list and check is num is devided by 7 and has a reminder, print(boom.)
"""
num = [0,1,2,3,4,5,6,7,8,9,10]
for i in range(1,11):
    if i % 7 == 0:
        print("boom")
    else:
        print("dammit")
"""

# Task = use the range function to loop through a code 6 times.
"""
num = [0,1,2,3,4,5,6,7,8,9,10]
for i in range(1,7):
    for i in num:
        print(i) 
"""


# Task = loop through the items in the num list and exit the loop when x is devided by 13 and hash no reminder.
"""
num = [0,1,2,3,4,5,6,7,8,9,10,11,12,13]
for i in range(len(num)):
    #syntax problem or else function won't work
    for i in num:
        print(i) 
        if i % 13 == 0:
            break
        else:
            print("no stepping down")
            break
"""
