#!/usr/bin/env python3

#import my_lib
# We imported the library called "my_lib" from earlier.

"""
When we are importing a library we also need to call ts functions. For example - the code below won't work as is because we didn't call it.
import my_lib
hello('Alex')
"""

from unicodedata import name
import my_lib
my_lib.hello('Alex')
# How do we call a function from a library? library_name.function_name('values') --> my_lib.hello('Alex)
# The library name is "my_lib", the function name is "hello" and the value is "Alex".

# Partial import
"""
In Python we have something called "Partial import".
Partial import is when we're importing a specific function from a whole library (just the function):
from my_lib import hello
hello('Alex')
We can also do --> from my_lib import * --> which will import all the functions in the library but this is not recommended.
"""

#"Dynamic importing/calling" of a function. (DETAILED IN THE "my_lib.py" FILE.)
"""
#if __name__ == "__main__":
# "__main__" is representing the place where we're running the script from, this is the main function that's running on Python behind the scenes.
# "__name__ == "__main__" --> we say that if the process we're currently running is the name of the main process --> I can operate and change a certain actions.
# By that we can autonatically run our function as soon as the main process is running.
"""

# We can give a name to a library and store it in a variable.
"""
import my_lib as panda
"panda" is the name we just gave to the library --> panda == my_lib.
"""


# Plus notes
"""
# "__name__" is a protected variable.
# "__" is called under/double-under.
"""
