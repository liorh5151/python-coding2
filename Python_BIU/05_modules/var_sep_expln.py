#!/usr/bin/env python3

########################
# THIS SCRIPT IS ABOUT DEFINING GLOBAL VARIABLES (VARIABLES FOR THE WHOLE SCRIPT). 
########################

# Arguments seperation (to see how the variable are seperated in Python).
"""
name = "Alex"
def echo(name):
    name = "Michael"
    print(name)
echo(name)
# when we'll run the function we will get the name "Michael" because there's a seperation between the areas in Python.
"""
# Another example:
"""
name = "Alex"
def echo(name):
    name2 = "Michael"
    print(name, name2)
# The result is "Alex Michael"
# A defined variable inside the function, is attached to only function itself (we can't touch it outside the function), but not the other way around.
"""

# import glob
"""
# In order to define global variable we can import a library called "glob" (memory consuming).
import glob # shortcut for global
# We can define a constant variable, which we can access anywhere (even from inside a function).
"""

