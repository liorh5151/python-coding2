#!/usr/bin/env python3

# THIS PYTHON FILE IS BEING USED AS A LIBRARY FOR A PYTHON SCRIPT CALLED "my_process.py".


from unicodedata import name


def hello(name):
    print('Hello {}'.format(name))

# We also have something called "Dynamic importing/calling" of a function. 
if __name__ == "__main__":
    hello(name)

# "__main__" is representing the place where we're running the script from, this is the main function that's running on Python behind the scenes.
# "__name__ == "__main__" --> we say that if the process we're currently running is the name of the main process --> I can operate and change a certain actions.
# By that we can autonatically run our function as soon as the main process is running.


# "__name__" is a protected variable.
# "__" is called under/double-under.





# Usually in Python we will import/define a number of libraries which we want to use in order to make our code as short and efficient as it possibly can.
