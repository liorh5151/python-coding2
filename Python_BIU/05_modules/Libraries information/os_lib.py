

# Here we can find information about a library calles "OS".
# Note - we need to import the os library only once per file, there's no need to import it every function.

import os 
# Everything we can do with our operating system (files and stuff).
# Library that allows Python to interface/connect with the operating system.
# Some capabilities:
"""
Manage memory
Run Bash commands
Run processes and supervise them
Configure settings
Pull things
Create/delete folders/directories/files/rename
Create/get an environmental variable
"""

# The main job of the os library is to connect/to be the gateway between the Python running wnvironment to the operating system Linux. 

# os.name()
"""
# "os" is the name of the library and "name" is a attribute beneath it.
# The attribute "name" used for telling us the operating system we are currently running.
# Possible answers for "os.name":
Linux --> posix/unix
Microsoft --> nt
# For example:
def os():
    import os
    print(os.name)
os()
"""

# os.lisdir()
"""
# "listdir" is an attribute beneath "os" library.
# The attribute "listdir" used for making a list of the files under a local files.
# For example:
def list():
    import os
    print(os.listdir())
list()
"""

# os.getcwd()
"""
# Is the same as "pwd" command in Linunx (Present Working Directory).
# Shows us the current directory we're working on. In other words a full path to where we are.
# For example:
def pwd():
    import os
    print(os.getcwd())
pwd()
"""

# os.mkdir('new_folder_name')
"""
# Create a directory (like mkdir in Linux).
# We need to deliver a string (a folder name we want to create) to the function and the function will create it accordingly.
# For example:
def mkdir():
    import os
    os.mkdir('new_folder_name')
    print(os.listdir())
mkdir()
"""

# os.rmdir('new_folder_name')
"""
# Do the opposite from the function "os.mkdir" (delete the specific file).
# We need to deliver a string (a folder name we want to delete) to the function and the function will delete it accordingly.
# We can delete a folder only if it's empty.
# For example:
def rmdir():
    import os
    os.rmdir('new_folder_name')
    print(os.listdir())
rmdir()
"""

# os.makedirs()
"""
# In order to create many sub-directoried under the same directory (not a tree). 
# We need to deliver strings (a folder names we want to create) to the function and the function will create it all accordingly.
# We can also create only one directory.
# For example: (to create a tree)
def makedirs():
    import os
    os.makedirs('new_folder_name1/new_folder_name2/new_folder_name3') 
    print(os.listdir())
makedirs()
#####################
# For example: (to create many under the same pwd)
def makedirs():
    import os
    os.makedirs('new_folder_name1','new_folder_name2','new_folder_name3') 
    print(os.listdir())
makedirs()
"""

# os.chdir()
"""
# The same as "cd" command in Linux (Change Directory).
# This function gets a srting and allows us to move between folders.
# We can also give a path to the function.
For sxample: 
def moving():
    import os
    os.chdir('desired_folder') 
    print(os.getcwd())
moving()
"""

# os.removedirs()
"""
# In order to delete many sub-directoried under the same directory (not a tree). 
# We need to deliver strings (a folder names we want to delete) to the function and the function will delete it all accordingly.
# We can also delete only one directory.
# For example: (to delete a tree)
def removedirs():
    import os
    os.removedirs('new_folder_name1/new_folder_name2/new_folder_name3') 
    print(os.listdir())
removedirs()
#####################
# For example: (to create many under the same pwd)
def removedirs():
    import os
    os.removedirs('new_folder_name1','new_folder_name2','new_folder_name3') 
    print(os.listdir())
removedirs()
"""

# os.environ
"""
# Prints all the environment variables.
# For example:
def environ():
    import os
    print(os.environ)
environ()
"""

# os.putenv
"""
# Allows us to create an environment variable only for the run time --> ('Key','Value')
# For example:
def putenv():
    import os
    os.putenv('name', 'Alex')
    print(os.environ)
putenv()
"""

# os.getpid
"""
# Returns the process ID for the process we are currently running.
# For example:
def getpid():
    import os
    print(os.getpid())
getpid()
"""

# os.getuid()
"""
# Returns the user ID for the user we are currently running.
# For example:
def getuid():
    import os
    print(os.getuid())
getuid()
"""

# os.getgid()
"""
# Returns the group ID for the group we are currently running.
# For example:
def getgid():
    import os
    print(os.getgid())
getgid()
"""

# os.system
"""
# Can run Bash commands.
# For example:
def system(): 
    import os
    os.system('git--version') --> os.system('bash_command')
system()
"""

# os.popen
"""
# Open process, create it, close it.
# It used to be the only function that can deal with processes.
# Using something calles "File descriptors --> fd
# For example:
def popen(): 
    import os
    os.popen(cmd) --> os.system(process_name/command)
popen()
"""