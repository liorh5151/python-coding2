# Here we can find information about a library calles "Subprocess".


import subprocess
from sys import stderr, stdin
#It lets you type commands right from the Python program you are currently writing.

####################

# subprocess.call('ls')
"""
# To run commands by strings (more information below).
# For example:
subprocess.call('ls')
"""

# When we define "shell", it helps to run more than one command/string.
"""
# For example:
subprocess.call('ls -la', shell=True)
"""

# We can also use the call function for a list.
"""
# For example:
subprocess.call(['ls', '-la'])
"""

####################

# subprocess.call
"""
# Does the same as subprocess.call(['ls', '-la']).
# For example:
subprocess.run(['ls', '-la'])
"""

####################

# os.Popen 
"""
# Open process, create it, close it.
# Upper "P" --> key sensitive.
# For example:
def subprocess(): 
    import subprocess
    subprocess.popen(cmd) --> subprocess.system(process_name/command)
subprocess()
"""

##################

# subprocess.Popen(['ls'])
# Subprocess.Popen([]) --> creating a new process/action/command.

# THIS IS THE FIRST OPTION SO REDIRECT INPUT, OUTPUT AND ERRORS.

process = subprocess.Popen(['cat', 'README.md'], stderr=subprocess.PIPE, stderr=subprocess.PIPE)
# We defined the variable "process" as the command "cat" to the "README.md" file.
# In order to redirect (like "|" in Bash script) the stdout and stderr --> stderr=subprocess.PIPE, stderr=subprocess.PIPE (info below),
# Subprocess.PIPE is a built-in function in the "subprocess" library which saves the binary value of stdout and stderr in the variable "PIPE".
# This option allow us redirect it to a different variables.

# process.communicate()
"""
# Returnes the stdout and stderr to the shell (prints).
# For example:
out,err = process.communicate()
"""

out,err = process.communicate()
# In order to edit tne information we need to save it in a variable in the memory --> stderr = err, stdout = out.
# The "process.communication" goes th the memory and take from the variable "PIPE" all the data in binary form (the data will be printed with "b" = binary).
 

# THIS IS THE SECOND OPTION SO REDIRECT INPUT, OUTPUT AND ERRORS.

process2 = subprocess.Popen(['grep', '-i', 'python'], stdin=process.stdout, stdout=subprocess.PIPE)
# Instead of doing redirection of stderr and stdout, we can do redirection of stdin.
# We basically say, take the input from the first process (subprocess.Popen(['grep', '-i', 'python']),
#  and the data you send to "process.stdout" --> stdin=process.stdout. AKA we took the input and placed it as output.
out = process2.communicate
# "out" is the stdout (output of the action).




