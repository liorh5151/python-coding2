
# Here we can find information about a library calles "Argparse".


import argparse

parser = argparse.ArgumentParser
# This is how we start the argument, now we can define settings to the argument.


parser.add_argument('-a', '--act', Default = 'Action has begun', help='This is useless action')
# This is how to pass an argument to a script.
# The "default" option is defining the variable calles "act" --> act = "action has begun"
# 'a' is a shortcut for "act" --> a = act
# W also defined an argument calles 'help' -->'help' = "This is useless action"
args = parser.parse_args()

print(args)

if args.act == "Action has begun":
    print('Default value has been initiated')
else:
    print(args.a)


# How to run a python script:
"""
python3 args.py 
We can also transfer arguments from the terminal
python3 args.py -a "Has the action begun?" --> a = "Has the action begun?"
"""
