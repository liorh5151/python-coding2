# Here we can find information about a library calles "Random".
# Generating random information based on the system.

import random 

# random.randint
"""
# Create a list of random numbers
# For example:
randint(1,100) --> 57,12,64
"""

# random.randrange
"""
# Define a list that we can use.
# For example:
start = 1
stop = 100
step = 2
randrange(start, stop, step) --> 60
"""

# random.choice
"""
# Can combine a number of variables, or strings.
seq = [1,2,3,4,5]
choice(seq) --> 3
"""