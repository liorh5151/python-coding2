#!/usr/bin/env python3

# This is the task:
"""
Create a script that inports "os" library.
Script should create 5 sub folders, one under other (like a tree --> sub1/sub2/sub3).
At the end the script should change to last sub folder and print current folder.
"""

# This is the answer for the task:

# This is the first wat to do the task:
"""
def task():
    import os
    os.makedirs('sub1/sub2/sub3/sub4/sub5')
    os.chdir('sub1/sub2/sub3/sub4/sub5')
    print(os.getcwd())
"""
# This is another way to do the task:
"""
def task():
    import os
    for num in range(1,6):
        os.makedirs('file{}/'.format(num))
    os.chdir(file(max(num))
    print(os.getcw())
"""
# This is another way to do the task:
"""
def task():
    lst = [1,2,3,4,5,6]
    import os
    from os import chdir
    from os import getcwd
    for num in lst:
        os.makedirs('file{}/'.format(num)
    os.chdir(file{num.max})
    print(os.getcwd())

"""
