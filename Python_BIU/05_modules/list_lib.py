#!/usr/bin/env python3

# In this file we will import a number of libraries in order to make our code as short and efficient as it possibly can.
# 3rd party libraries --> libraries that Python gives "out of the box"

import os
# Everything we can do with our operating system (files and stuff).

import sys
# A system library fot Python. For example getting variables and definning them.

import datetime
# Change/manipulate dates. (date command in bash)

import math
# Library that allows to do math calculations.

import random
# Library that generated random object (by our need or by the operating system's need, easy for encryption)

import base64
# Library that encodes binary data to Base64, encoded format and decode such encodings back to binary data.

import socket
# Library that opens a communication channel by TCP/IP or UDP/IP protocols.

import requests
# Create + accept HTTP/HTTPS requests (tool in Linux called "curl").

import re # Regular Expressions (called "crap" in bash).
# 

import json
# Library that knows to convert json content to Python dictionary and the other way around (not only json/dict).

import yaml
# Same as "json" library but in a different standard called "yaml".

import xml
# 

import argparse
# Library that allows to spread arguments into the script (like getopts in linux).
# positional arguments in bash script
# getopts in python ?
# the soluting to positional arguments called --> argparse library.

import sqlite3
# Library that allows to create a Data base.

import random
# Give random data.

import subprocess
#It lets you type commands right from the Python program you are currently writing.

# PyPI
"""
 except 3rd party libraries, we can also get libraries from a place called "PyPI" (Python Package Index).
 PyPI is a website/repository that stores a Python packages that other people wrote over time.
 We can download and install packages from PyPI and use it in our code.
 Python packages called "eggs" (because the Python logo is a snake).
 #pip
 Instead of download it from the website, there's a built in code in Python called "pip" (this is the apt-get for Python libraried).
 To download using pip we will do --> pip3 install library_name --> pip. install flask (flask is the library name).
"""



# Plus notes
"""
 Flask is a collection of libraries that allows us to build applications.
 Framework is a collection of libraries.
 """