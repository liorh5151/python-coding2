
# This is an explanation about the "try + except" function in Python.

import logging

# For example:
"""
# We know that any number divided by zero (0) is an error.
# We can do 'try' and 'except' in order got the script to try the first thing (try) and in case it doesn't work, do the other thing (except):

try:
    1/0

except:
    logging.error('Exception has occurred', exac_info=True)

""" 

