# This is the flask task.

# flask
- create working enviroment:
    - install pipenv
    - configure pipenv with:
            - flask
            - ipython
            - gunicorn
            
    - create app.py file and initialize app:
        - the application should have 3 paths:
            - / that welcomes to flask app
            - /hello/<username> that welcomes the specific user
            - /sys_data that use libs below to print out system information:
                - os
                - sys
                - platform
    
    - Homework:
            - /create_db that will create sqlite3 db once it will be called.
                - it should return notification if db already exists.
                - it should return notify if success or not.
            - /sys_user that prints the username of the os you are working with.