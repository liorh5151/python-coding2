

import os
import sys
import sqlite3
from os.path import isfile
import platform # data on the operating system
from flask import Flask


app = Flask(__name__)

template = """
<h1 style='color: red'> Hello! welcome to my flask app! </h1>
"""


@app.route('/')
def index():
    return template 

@app.route('/hello<username>')
def hello(username):
    return f"hello {username}! we're glad your here :)"

@app.route('/system_data')
def data():
    data = (sys.version, os.environ, platform.platform())
    return str(data)
    
@app.route('/sys_user')
def user_name():
    user_name = print(os.getlogin())
    return str(user_name)

# db
@app.route(' /create_db')
def database():
    con = sqlite3.connect('file:app.db?mode=ro', uri=True)
    

def isSQLite3(app.db):
    if not isfile(app.db):
        return "Sorry, we could not find this database..."
    else:
        return "This database is already exists!"

    

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080, debug=True)
