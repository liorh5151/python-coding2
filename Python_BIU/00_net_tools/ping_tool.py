#!/usr/bin/env python3

# In this file we can find a tool meant for IP address discovery.

# The task:
"""
Make tool to work with arguments
Implement python knowledge to create tool that can list all intrefaces
Use modules to ping some address or domain name
Make module ipaddress (library) to test if the ip is part of some specific network or not.
"""


# This is the answer for the task:
import ipaddress
import netifaces
import socket
import nmap

# Requirements file content
"""
pip install netifaces
pip install nmap 
"""


def usr_menu():
    while True:
        print(""" 
        Please select one of the following options:
        1. Information for an IP address.
        2. Information about a domain name.
        """)
        usr_choose = str(input("Please choose an option."))

        if usr_choose == "1":
            ip()
        elif usr_choose == "2":
            domain()



def ip():
    ip_address = str(input("Please put IP address."))
    subnet_mask = str(input("Please enter subnet mask in the following format --> /x."))
    full_ip_address = ip_address + subnet_mask

    if ipaddress.ip_address(ip_address) == IPv4Address(f'{ip_address}'):
        print(""" 
        Please select one of the following options:
        1. Network hosts scan
        2. Network Interfaces scan.
        3. IP addresses inside the network
        4. Additional port scanning.
        5. Exit
        """)
        usr_ans = input("Please choose an option.")
        if usr_ans == "1":
            print(f"These are the details for the ip address {ip_address}{subnet_mask}:")
            ip_hosts = list(full_ip_address.hosts())
            for host in ip_hosts:
                print(host)

        elif usr_ans == "2":
            print(f"These are the details for the ip address {ip_address}{subnet_mask}:")
            # list of interfaces
            interfaces = netifaces.interfaces(ip_address)
            for interface in interfaces:
                print(interface)

        elif usr_ans == "3":
            print(f"These are the details for the ip address {ip_address}{subnet_mask}:")
            all_ip =  ipaddress.IPv4Network(ip_address)
            print(all_ip)


        elif usr_ans == "4":
            scanner = nmap.PortScanner()
            host_name = scanner[ip_address].hostname()
            print(f"These are the details for {host_name}:")
            scanner.scan(ip_address, ports= '1-1024', arguments= 'Pn -p-')
        
        elif usr_ans == "5":
            print("\n We're sorry to see you go!")
            
        else:
            return "Sorry, this is not a valid answer..."
        
    else:
        print("Sorry, only IPv4 is allowed...")



def domain():
    input_domain_name = str(input("Please put your domain name."))
    domain_ip = socket.gethostname(input_domain_name)
    
    if 'http://' or 'https://' in input_domain_name:
        print(""" 
        Please select one of the following options:
        1. Domain IP address.
        2. Domain Interfaces scan.
        3. IP addresses inside the Domain's network.
        4. Additional port scanning.
        5. Exit.
        """)
        usr_ans2 = input("Please choose an option.")

        if usr_ans2 == "1":
            print(f"These are the details for the domain name {input_domain_name}:")
            print(domain_ip)

        elif usr_ans2 == "2":
            print(f"These are the details for the domain name {input_domain_name}:")
            interfaces = netifaces.interfaces(domain_ip)
            for interface in interfaces:
                print(interface)

        elif usr_ans2 == "3":
            print(f"These are the details for the domain name {input_domain_name}:")
            all_ip =  ipaddress.IPv4Network(domain_ip)
            print(all_ip)

        elif usr_ans2 == "4":
            print(f"These are the details for the domain name {input_domain_name}:")
            scanner = nmap.PortScanner()
            host_name = scanner[domain_ip].hostname()
            print(f"These are the details for {host_name}:")
            scanner.scan(domain_ip, ports= '1-1024', arguments= 'Pn -p-')

        elif usr_ans2 == "5":
            print("\n We're sorry to see you go!")
            
        else:
            return "Sorry, this is not a valid answer..."
    else:
        return "Sorry, this is not a valid answer..."


if __name__ == '__main__':
    usr_menu()