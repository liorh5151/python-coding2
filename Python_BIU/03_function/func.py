#!/usr/bin/env python3

# Function

# In order to define/create a function --> def function_name(function_var)
# For example: def numbers(1,2,3,4,5)

"""
def hello():
    print('Hello world')
# We can't activate a function by creating it, we also need to call it (due to sub-memory)--> for example - function_name()
hello()
# (In bash script we don't need to use () in order to activate a function) 
"""

"""
# Arguments = the value we deliver to the function (the variables)
def hello(name):
    print("hello" , name)
hello("Lior")
# By doing that we configure the argumant name as Lior --> name = lior
# We can also configure an argument like this: def hello(name = "lior")
# For example --> (below)
def hello(name = lior):
    print("hello" , name)
hello()
# Now the argument name is set as "Lior" by default.
# If we will do the following action --> hello('Alex') --> the value "Lior" as name will be gone and "Alex" will replace it.
"""

"""
# Arbitrary argument --> a situation where we need to transfer a huge amount of arguments into a function
# How? We will define a function to get a list.
def hello(*name):
    print('hello' , name[1])
hello('Alex', 'Mark', 'Michael')
"""
 
"""
# KeyWork Argument --> Key;Value
def choose(**name):
    print('the name is ' , name['fname'])
choose(fname = 'Alex', lname='Cohen')
"""


# In bash script all the arguments (var) are global, which is different from python.
# In python the arguments inside and outside the function are different.
# In order to define an argument as global we will do the following option --> global argument_name 
# For example: 
# x = 5
# global x (we cam also set the argument for every single function).


"""
# Key arguments
def my_function(*names):
    for name in names:
        print("hello", name)
# We are defining the argumant names as the list ninga, pirate and noob --> names = ('ninja', 'pirate', 'noob')
my_function('ninja', 'pirate', 'noob')
"""

# Data collection in Bash --> array --> $@ || $*
# Bash --> 1> 2> <3 (Data redirection)
# Bash --> cat /etc/*-release | grep ID


# Key Word Arguments --> AKA kwargs and is set by ** (example below)
# Transfer Key;Value pairs into functions
"""
def my_function(**names):
    for fname,lname in names.item():
        print('hello', fname, lname)

my_function('fname':'Alex', 'lname':'Cohen')
"""
# Dictionaries --> holds key&value. For example: --> dct = {fname:'Alex'}
# type(dct --> dict)

"""
def add(x):
    print(x*5)
add(5)
value = add(5)
#type(value) --> NoneType.
# Why? VOID. A hole where you can't do or define anything, it's a black hole.
# We get a NoneType valSue when we define a function which returns no value.
# How to fix it? we will go back to the function and use a python saved word. In this example we will use return:
def add(x):
    print(x*5)
    return x*5
type(value) --> int.
"""

# Place holder = the ability to save undefined functions(in order to keep it in the memory).
# Pass --> Ignore the function.
"""
def dunction():
    pass
"""

# Recursive --> functions that calls itself --> function_name()
"""
def hello(name = 'you'):
    print(f'hello{name}')
    hello()
"""

# .Format() in function
"""
# The .format() will take out input and out it inside the --> {} --> see example below:
print("Hello world!")
print("Hello {}".format(name))
# The format option will the "name" and put it inside Hello {} --> print("Hello {name}")
"""

# Lambda function --> anonymous function (no memory slot is saved by it).
"""
# It's a one time function that has no saved memory space. 
# How to use lambda --> lambdas vlaue: expression:
# Lambd is a built-in python function (built-in word)
x = lambda a: a * a
#x is a variable
x(5) --> 25
"""
