

- Create a function named do_function that prints its own name. Execute the function.
- Create a function named do_function that prints any name variable passed to it. Execute the function.
- Create a function named do_function that prints four name variables passed to it. Execute the function.
- Create a function named do_function that prints variable 4 times and also returns it, that were passed to it. Execute the function.
- Create a function named do_function that accepts any number of variables prints them one by one in one line. Execute the function.
-  Create a function named do_function that accepts values passed to it as key/value pairs and prints all keys and values one next to each other. Execute the function
- Write a Python function to find the Max of three numbers
- Write a Python function to sum all the numbers in a list
- Write a Python function to multiply all the numbers in a list
- Write a Python program to reverse a string
- Write a Python function that takes a list and returns a new list with unique elements of the first list
- Write a Python function to check whether a number is perfect or not. check [link](https://en.wikipedia.org/wiki/Perfect_number) to know what is `perfect number`.
- Write a Python function to check whether a string is a pangram or not. [link](https://en.wikipedia.org/wiki/Pangram)to learn what pangram is.
- Write a Python function to check whether a string palindrom or not. what is [palindrom](https://en.wikipedia.org/wiki/Palindrome) ?   Nisio Isin --> Murder for a jar of red rum

# The answers to the tasks are stored in the file "ans.py".