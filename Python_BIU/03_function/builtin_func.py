#!/usr/bin/env python3

# These are well known built-in functions:
"""
print() --> print("Hey") --> Hey
help() --> help(print) --> manual for print function
len() --> calulate the lengh of a vlaue --> len("Hey") --> 3
format() --> takes a string and enter a values according to what we defined
"""
# Change casting functions:
"""
int() --> int('5') --> 5 | type(5) --> int
str() --> str(5) --> '5' | type(5) --> str
float() --> float(5) --> 5.0 | type(5) --> float
"""
# Data collections functions:
"""
list() --> x = list() --> x is a list
set() --> x = set() --> x is a set
tuple() --> x = tuple() --> x is a tuple now
dict() --> x = dict() --> x is a dictionary now
"""
# Numbers/Math functions:
"""
range(start;stop;step/jump) --> represent a range and can be used with a Deata collection functions (such as above). Step by default is 1.
max([]) --> takes out the max value --> max([1,4,3,2]) --> 4
min([]) --> takes out the min value --> min([1,4,3,2]) --> 1
sum() --> calculate the sum of given arguments --> sum([3,4]) --> 7 (you can put as many as arguments as you wish in as list/set/tuple/dict).
pow() --> calculate two numbers when the first one is a power number of the second one --> pow(5,2) --> 25
"""
# Binary information functions:
"""
bin() --> turns any binary number into a string (with the letter b inside for bin) --> bin(5) --> '0b101'
hex() --> turns any hexadecimal number into a string (with the letter x inside for hex) --> hex(5) --> '0x101'
oct() --> turns any octaly number into a string (with the letter o inside for oct) --> oct(5) --> '0o101'
""" 
#ASCII functions (and some additional info):
"""
# Ansi C standard character code - what nakes Python & Linux key sensitive AKA ASCII
# Today python is using UNI-code (which makes Python multi-platform and operate also on both Windows & Linux due to using both ASCII & UNI-code)
ord() --> turns the letters into numbers by ASCII UTF standard --> ord('a') --> 97
chr() --> turns the numbers into letters by ASCII UTF standard --> ord(97) --> 'a'
"""
# Magic functions:
"""
map, filter, reduce, Zip --> a collection of actions on many objects at the sane time. For example:
def myfunc(x):
    return x**x
my_list=[2,3,4,5,6]
# we will give the function(myfunc) and the list name(my_list) and use the map function in order to execute the action, the result will be saved in another argument.
result = map(myfunc, my_list) --> result = 0x7f6343548
"""


# Mathemayical basis:
# decimal 0-9 (base 10)
# binary 0-1 (base 2)
# hexadecimal 0-f -->0123456789abcdef (base 16)
# octal 0-7 (base 8) --> the command chmod using octal base to change Linux file permissions, when 7 is the top/max permissions --> Read+Write+Edit.
