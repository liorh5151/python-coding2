#!/usr/bin/env python3

# These are my answers to the function tasks in file "tasks.md".


# Function number 1:
# This is an example
"""
def do_function():
    print(do_function.__name__)
do_function()
# __name__ represent the name of function.
"""
# This is the actual task:
"""
def do_function():
    name = "do_function"
    print(name)
do_function()
"""


# Function number 2:
# This is an example
"""
def do_function():
    x = 5
    print(dir())
do_function()
# locals() prints all the local variables. In order to see variables --> dir(), locals() or globals().
"""
# This is the actual task:
"""
def do_function():
    x = 5
    print(dir())
do_function()
"""


# Function number 3:
"""
def do_function():
    x = 5
    y = 2
    z = 1
    r = 9
    print(dir())
do_function()
"""


# Function number 5: need to continue

def do_function():
    x = 5
    y = 2
    z = 1
    r = 9
    fun = dir()
    #print(fun , sep='', end='\n')
    print(dir() , sep=',', end='\n')
do_function()    