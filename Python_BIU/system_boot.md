
# THESE ARE THE STAGES EVERY LINUX COMPUTER GOING THROUGH WHEN BOOTING:
(Processes tree until fully working operating system).

1. Boot (Power-on).

    2. POST (Power Over Self Test).
    When the system is checking the hardware.
    The motherboard checks that all the cheaps etc can handle and work.
    (We hear it as a "beep" sound coming out from out computer).

        3. BIOS (Basic Input Output System).
        BIOS activates something called:

            4. Sector Zero .
            A list of processes in order to boot.

                5. Grub (Grand Universal Boot Loader).
                A tool which checks which kernel is on the HDD (Hard Drive).
                Afterwards, the tool will activate the operating system.
                Grub runs the:

                    6. Kernel (Which is running on the RAM).

                        7. System
                        Running the tool who is manage the operating system.
                            systemd - written in Bash script.
                            init.d (older operating systems) - written in C programming language.

#####################################################################################################

# The first process in the booting of the computer:

Process ID number 1 in Linux operating systems will always be systemd/init.d 
We will focus on system.d

1. systemd
    when systemd is eunning, it acticates something called:

    2. Unit 
    The basic unit helps the operating system to know which resources it has and how to use them.
    All the units in Linux are under /usr/lib/systemd
    There we can find filed ending with *.service/*.timer/*.socket/etc. (Service used to be called as daemon)
    Every file is a unit (and also a process) and the system priorities when by usage for the things we choose to use.
    Eventually every command is turnning into a process, from the beginning until the end of the process.

#########################################################################################################

# For example, let's take the "ls" command in Linux and see the process behind it:

    1. ls
    We're typing the command.

        2. New process is being creates.
        A process ID is given.

            3. Sub-shell

                4. Calculations

                    5. Current-shell
                    which knows to take out the:

                        6. stdout
                        and prints us the results

##############################################################################################################

# How every process is running behind the scenes in linux:

    1. Shell
    This is the parent process ID of the (PPID)
 
        2. Sub-shell

            3. exec
            Command to run our action/process

                4. PID (Process ID)
                This is the child process ID of the 

                    5. Kermel
                    When we get the PID, the process is being transfered to the kernel

                        6. Binary data
                        The binary data will be transfer back to the:

                            7. PPID (Parent Process ID) --> shell.
                            We can recreate the whole process by something calles "parsing":
                            Parsing --> Data Mapping into standard output.

##############################################################################################################

# The process of running Python in Linux;

    1. Shell

        2. Python

            3. Sub-shell
            
                4. Python run environment

                    5.Byte-code
                    The script we wrote is turnning into something calles "byte-code"

                        6. Kernel

                            7. PPID (Parent Process ID) --> shell.
                            The data is being transfered to the PPID whic is the shell.
                            The process is returning to the shell we execute the command from.


