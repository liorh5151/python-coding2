

###########################
# Creating a web application using "flask" collection of libraries.
###########################

import os
import sys
import logging

# After importing sqlite3 we can start and defining databases 
# sqlite3 is a library that exists in every programming language.
import sqlite3

# The collection called "flask" is creating a URLs which we can reach on the web.
from flask import Flask


logging.basicConfig(filename='app.log', filemode='w', level=logging.DEBUG)

app = Flask(__name__)
# We defined that the name of the application is "app", 
# Because Flask is the name ("__name__") of the application and we defined it as "app".
# We have to use this option every time we run flask.

# URL (Unifies Resource Locator) --> https://{DNS address} --> webserver --> application (we see it as a URL).
# Flask will generate URL paths by http protocol, in "flask" we have a built-in web server.
# We will create something called "decoratore" --> "@app" and by that we can reach our application.

############################

# this is one way to run the application (we can put many html code inside)
template = """
<h1 style='color: blue'> Hello world</h1>
"""

# The "@app.route('/')" is how we define a path.
# Here we defined that we want to get to out app from the path "/" --> path = /.
@app.route('/')
def index():
    return template # We can also do a string which we will see when we enter the site

# We are now creating a database
@app.route('/create_db/<db_name>')
def create_db(db_name):
    # We are now creating a connection to the database and store it in a log
    logging.info(f'On it! creating DB {db_name}.')
    # We are now creating a connection to the database
    connection = sqlite3.connect(db_name)
    # Here we define the log for the DB creation
    logging.info(f'DB {db_name} created.')


# How ot transfer parameters to a path.
@app.route('/hello<user>')
def hello(user):
    return f"hello {user}"
# we will define the "user" name by typing it in the web window (URL window).
# For example: 0.0.0.0:8080/hellolior --> hello lior

@app.route('/system_data')
def data():
    data = (sys.version, os.environ)
    return str(data)
# We define index function so we can say what we want to see then we get there (to the website).
#def index():
#    return "Hello world"

"""
def index():
    data = (sys.version, os.environ)
    return str(data)
"""

# The application will output every login (for example: 127.0.0.1 - - [20/Feb/2022 11:49:54] "GET / HTTP/1.1" 200 -).

#if __name__ == "__main__":
    #app.run()
# we have options below the "app.run()", like changing the port number --> app.run(host, port, debug).
# host is the ip address, for example --> app.run(host='0.0.0.0')
# port is the access port, for example --> app.run(host='0.0.0.0', port=8080)
# debug is the boolean value (T/F) and gives the option to run the application even if there's something wrong with the code --> app.run(host='0.0.0.0', port=8080, debug=True)

############################

# This is second way ro tun the application:

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080, debug=True)
# Instead of using these two lines we can write the commands ob the terminal --> "flask run". 
