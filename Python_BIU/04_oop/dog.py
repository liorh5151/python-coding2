#!/usr/bin/env python3

"""
- create class dog with :
    - attributes:
        - name
        - age
        - sex (f/m)
    - methods:
        - sound - make her/him houl
        - sleep - print that s/he is going to sleep
        - up - print that s/he wokeup
        - hungry - return boolean if yes or no, should by dependend if slept or no
"""
class Dog:
# Class if always defined by uppercase in the first letter (Dog).
    def __init__(self, name='', age='', sex=''):
        self.name = name 
        self.age = age
        self.sex = sex
        self.hungry_status = True

    # How to define methodes: (Down below)

    def sleep(self):
        self.hungry_status = False
        print("Roxy go sleep now")

    def sound(self):
        return 'awuuu'

    def up(self):
        self.hungry_status = True
        print("Ooops, Roxy is up...")

    def hungry(self):
        if self.hungry_status:
            return f'The hunger status is {self.hungry_status}'
        else:
            return f'The hunger status is {self.hungry_status}'


Rexy = Dog('roxy' , 5 , 'm')
print(Rexy.sound())
print(Rexy.sleep())
print(Rexy.up())
print(Rexy.hungry_status())

# We have to put 'self' in order to connect the 'def' to the class.
        


    