
- create class dog with :
    - attributes:
        - name
        - age
        - sex (f/m)
    - methods:
        - sound - make her/him houl
        - sleep - print that s/he is going to sleep
        - up - print that s/he wokeup
        - hungry - return boolean if yes or no, should by dependend if slept or noy

# THE ANSWER FOR TASK IS IN ANOTHER FILE CALLED "dog.py".

- create class cat with:
    - attributes:
        - name
        - age
        - sex
    - methods:
        - sleep : run only if attribute name is passed
        - jumpy : if run function, sex needs to be male

# THE ANSWER FOR TASK IS IN ANOTHER FILE CALLED "cat.py".


- create class animal with:
	- attributes:
		- name
		- gender
		- breed
	- methods:
		- speak: general method for making a sound
		- getallinfo: get's all the parameters of both classes

	- create class Dog that inherits animal class, with :
		- set the sound of dog
		- set gender
		- set breed

	- create class Cow that inherits animal class, with:
		- set sound
		- set set gender
		- set breed

# THE ANSWER FOR TASK IS IN ANOTHER FILE CALLED "animal.py".


