#!/usr/bin/env python3

class Cat:
# Class if always defined by uppercase in the first letter (Dog).
    def __init__(self, name='', age='', sex=''):
        self.name = name 
        self.age = age
        self.sex = sex
        self.hungry_status = True

    def sleep(self):
        if (len(self.name)>1):
            print("cat goes asleep")


    def jump(self):
        self.sex = 'female'
        print("Cat is jumping")    

Dora = Cat('Dora' , 5 , 'female')
print(Dora.sleep())
print(Dora.jump())

# We have to put 'self' in order to connect the 'def' to the class.
        
    