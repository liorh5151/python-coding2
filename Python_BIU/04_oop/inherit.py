#!/usr/bin/env/ python3

# Now we will create a parent class for the rest of the classes:
class Bird:
    def __init__(self):
        print("Bird is ready")
    
    def who_is_this(self):
        print("Bird")
# "self" is the ability of the class to own data, attributions. 
    def swim(self):
        print("Swim Faster")

# In order for the Penguin class to inherite the Bird class we will have to put it in the "()" --> see example below.
# When we inherite a class we take the startup func (__init__) and everything attached to it (the functions).
class Penguin(Bird):
    def __init__(self):
# Although we took all the data from the class Bird, we need to startup the Penguin class itself (with the Bird data).
        super().__init__()
# The built-in function super that returns a temporary object of the superclass,
# that allows access to all of its methods to its child class (a simpler explanation below)
# The "super().__init__()" in the class also startup the current class and inherties from the Bird class all the methodes and attributes.
        print("Penguin is Ready")
    
    def run(self):
# We can also add methodes
        print("run faster")

# How do we start the classes?
linus = Penguin()
linus.who_is_this()
linus.swim()
linus.run()
# The "linus" built-in function 

