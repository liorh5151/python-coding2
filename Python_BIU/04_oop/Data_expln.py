#!/usr/bin/env python3


# Every data holda a place in the RAM.
# Every variable defining in Python is static.

# Data collection--> capability of saving data in specific manner in RAM
# Access to a specific section in the RAM which holds a certain amount of data.

# List = type of Data collection

# Object/Data structure = custom Data structure that has its own varibales and function
# var = 'Alex'

# Template --> name, lname, function

# Object Oriented Programming (OOP)


# 4 bits == "word"
# Python defining things by 4 bits (AKA words).

# Every data collection is a temple for another Data collection
"""
my_list = list ()
type(my_list) --> list
"""

# OOP --> Paradigm

"""
Alex = Person

class person():
    def __init__(self, name, lname:
        self.name = name
        self.lname = lname
"""

"""
class Person():
    def __init__(self, name=None, age=0, hobby=[]):
        self.name = name
        self.lname = lname
        sefl.age = age
        self.hobby = hobby

Alex = Person()
Alex = Person('Alex', 'Cohen')
Alex.name --> 'Alex'
Alex.lname --> 'Cohen'
Alex.hobby = ['salsa', 'meat', 'comics'] || Alex.hobby --> 'salsa', 'meat', 'comics'.
"""


