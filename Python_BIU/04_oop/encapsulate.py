#!/usr/bin/env python3
"""
def hello(name):
    print(f'hello {name}')
"""

class Computer:
    def __init__(self):
        self.__maxprice = 8000
# We're defining a hidden variable "maxprice = 8000". 
# The "__" in the hidden variable gives protection to the variable, no one can overwrite/destroy it now.

    def sell(self):
        print("Selling price {}".format(self.__maxprice))
    
    
    """
# Here we can see that the variable "maxprice" is really protected with the "__" configuration, the price will always be 8000.
comp.__maxprice = 25000
comp.sell()
    """

# settering & gatharing --> is the ability to ask a class to change a value in its structure:
    def set_new_price(self, price):
        self.__maxprice = price #explanation below.
"""
# Here we are trying to change the "naxprice" value in another way. 
# This time Python lets us get to the value and redefine it but still not letting us overwrite it.
comp = Computer()
comp.sell()
comp.__maxprice = 25000
comp.set_new_price(25000)
comp.sell()
"""

"""
#  How do we start the class:
comp = Computer()
comp.sell()
"""