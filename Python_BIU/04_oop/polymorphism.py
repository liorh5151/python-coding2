#!/usr/bin/env/ python3

class Parrot:
    def fly(self):
        print('Parrot can fly')

    def swin(self):
        print('Parrot can NOT swim')


class Penguin:
    def fly(self):
        print('Penguin can NOT fly')

    def swin(self):
        print('Penguin can swim')

    

def flying_test(bird):
    bird.fly()

linus = Penguin()
jako = Parrot()

flying_test(linus)
flying_test(jako)


