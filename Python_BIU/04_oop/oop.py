#!/usr/bin/env python3

class Person:
    def __init__(self, name='', lname='', age=0, hobby=[]):
        self.name = name 
        self.lname = lname
        self.age = age
        self.hobby = hobby
    
    def list_hobbies(self):
        for hobby in self.hobby:
            return f'I love to {hobby}'

# Another example:
    def full_name(self):
        for self in self.name:
            return f'{self.name} {self.lname}'

Lin = Person('Linuy', 'Ben-David', 21, ['TV', 'PUZZELS', 'sudoku'])
Lin.full_name()

print(Lin.full_name())
print(Lin.list_hobbies())
