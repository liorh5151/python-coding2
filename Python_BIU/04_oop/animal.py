#!/usr/bin/env python3

# THIS IS THE MISSION'S INSTRUCTIONS:
"""
- create class animal with:
	- attributes:
		- name
		- gender
		- breed
	- methods:
		- speak: general method for making a sound
		- getallinfo: get's all the parameters of both methodes

	- create class Dog that inherits animal class, with :
		- set the sound of dog
		- set gender
		- set breed

	- create class Cow that inherits animal class, with:
		- set sound
		- set set gender
		- set breed
"""
# THIS IS THE ANSWERS FOR THE MISSION:
class Animal:
	def __init__(self, name='', gender='', breed=''):
		self.name = name
		self.gender = gender
		self.breed = breed

	def speak(self, sound):
		self.sound = sound
		print(self.sound)
	
	def getallinfo(self):
		print(locals())

class Dog(Animal):
	def __init__(self, name='', gender='', breed=''):
		super().__init__()
	def speak(self):
		print("Awww")	
# There are 4 arguments inside "def___init__" because I kept getting error msgs.
class Cow(Animal):
	def __init__(self, name='', gender='', breed=''):
		super().__init__()
	def speak (self):
		print("meoww")

	

Rocky = Dog('Pocky','m','yorki')
Moka = Cow('Moka','f', 'siami')
Rocky.speak
Rocky.getallinfo
Moka.speak
Moka.getallinfo
	




