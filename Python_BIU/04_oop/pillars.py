#!/usr/bin/env python3

# 4 pillars of OOP (Object Oriented Programming) --> Ideas that OOP relay on:
"""
1. Abstraction
2. Inheritense
3. Encapsulation
4. Polymorphism
"""

###############
# 1. Abstraction
###############
"""
Abstraction is when we're developing a tool that when we call it, it will operate behind the scenes, without the user knowing it.
my_list = []
Right now we only know that we defined a list, but we cant tell what's really hapenning behind the scenes,
we know that list is a type of a Data collection in the memory, we can't really tell the changes and the steps behind it.

In OOP there's also an Abstraction layer.
For example, when we define a class (like we did earlier with a class named "Dog")
There are some processes that we are now aware of, like when the Class is saving a space and saving the variables in it.
"""
###############
# 2. Inheritense
###############
"""
Inheritense is when we Inherite inforamtion from other classes.
With the command "import" we can inherite other libraries that other prople wrote, for example --> import library_name
When you Inherite from other Data Collections, the action will effect only you.
"""
###############
# 3. Encapsulation
###############
"""
Encapsulation is when we wrap the data in a frame that allows it to exist as a single object with abilities of its own.
Encapsulation is the ability of an object to manage its state however it wants,
For example moving its hosting memory to another place or moving the object itself in order to save memory space.
"""
###############
# 4. Polymorphism
###############
"""
Polymorphism is the ability to change an object shape by existence of a certain conditions.
For example we have a class and we have a conditions. We also have a condition to check if we have these conditions -->
In this case, this object can help us make sure that these conditions are existing in the class we created.
For example - if the cat jumps than the cat's gender mush be female.
"""




