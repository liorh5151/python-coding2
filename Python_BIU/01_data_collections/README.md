# Data Collenction: memory use to saving diffrent types of data
## List, Tuple, Set, Dict
### List
lst = []
type(lst)
lst.append(1)
lst.append(2)
lst.append(3)
lst
lst.insert(1,100)
lst
lst2 = [5, 6, 7]
lst3 = lst + lst2
lst3
lst.extend(lst2)
lst
lst.sort()
lst
import os
os.system('clear')
import os
os.system('cls')
lst
lst.pop()
lst
lst.pop(0)
lst
lst.remove(5)
lst
lst.clear()
ls
lst
del lst
lst
lst3
lst3.reverse()
lst3
lst3.sort()
lst3
lst = lst3.copy()
lst
lst == lst3
lst4 = lst3
ls4
lst4
lst.index(100)
lst.index(12)
lst
1  in lst
12  in lst
# loop: while and for
# for each loop: indexing each element in list
for element in lst:
    print(element)
for element in lst:
    print(element,end='\t')
element
# iterator
# dynamic list generation: list comprehention
lst
for i in lst:
    if i % 2 == 0;
 4 / 2
 5 / 2
 5 % 2
 5 % 2 == 0
 4 % 2 == 0
True/1
False/1
1/False
for i in lst:
    if i % 2 == 0:
        lst5.append(i)
lst5
lst5 = []
for i in lst:
    if i % 2 == 0:
        lst5.append(i)
lst5
lst6 = [i for i in lst if i % 2 == 0]
lst6
# list comperehention
5 % 5 == 0
5 % 4 == 0
5 % 4
1 == 0
for i in lst:
    if i % 2 == 0:
        lst5.append(i)
# Data Collection
## tuple
### unmutable data structure
var  = 1
var
var = 2
lst
lst[0]
lst[2]
lst[2]=200
lst
tpl = (1,2,3,4)
tpl
tpl[0]
tpl[3]
tpl[3]=100
for i in tpl:
    print(i)
for i in tpl:
    print(i,end='\t')
tpl=(4,3,2,1)
tpl=(lst)
tpl
type(tpl)
tpl=(4,3,2,1)
type(tpl)
tpl[0]=21
abra_cadabra=()
type(abra_cadabra)
tpl
tpl[0]
tpl[-1]
tpl[0:2]
del abra_cadabra
# packing to tuple
# unpack tuple
var , var2, var3, var4 = tpl
var
var2
var3
var4
tpl
var = 100
tpl
var
tpl
var, *var2 = tpl
var
var2
(var, *var2) = tpl
# args kwargs
tpl2 = (5,6,7,8)
tpl3 = tpl + tpl2
tpl3
tpl3.index(1)
tpl3.index(4)
tpl3.count(4)
tpl4(1,1,1,1,1,2,2,2,3,3,5,5,5,5,5,5,5,5,7,7,7,7,7)
tpl4=(1,1,1,1,1,2,2,2,3,3,5,5,5,5,5,5,5,5,7,7,7,7,7)
tpl4.count(7)
tpl4.count(1)
tpl4.count(2)
tpl4.count(3)
tpl4.count(5)
tpl4.count(7)
tpl4.count(6)
for i in tpl4:
    print(i)
for i in tpl4:
    print(i,end=',')
# Data Collection:
## Set
my_set = set()
type(my_set)
my_set = {1,2,3,43,4,5,,23,44,23,1,1,,2,34,4}
my_set = {1,2,3,43,4,5,23,44,23,1,1,2,34,4}
my_set
type(my_set)
my_set[2]
for i in my_set:
    print(i)
2 in my_set
my_set.add(100)
my_set
my_set2= {8,9,11}
my_set.update(my_set2)
my_set
my_set.update(1)
my_set2= {1,2,3}
my_set.update(my_set2)
my_set
my_set.remove(1)
my_set.
my_set
my_set.remove(9)
my_set
my_set.pop(2)
my_set.pop()
my_set.pop()
my_set.pop()
my_set
my_set.pop()\
my_set.discard(11)
my_set
my_set.pop()
my_set
my_set.discard(100)
my_set.pop()
my_set.pop()
my_set.pop()
my_set.pop()
my_set.pop()
var = set()
var = {1,2,3,4,5,6,7,}
var
var.pop()
for i in var:
    print(i)
var2 = var.union(my_set)
var2
# List
# set : list with unique values
# tuple: list that can not be changes
lst = [1,1,1,1,2,2,2,2,3,3,3,3,3,3,3,5,5,5,5,4,4,4,4,4,6,6,6,6]
lst
var = set(lst)
var
var = list(var)
var
# Data Collections
## Dictionary
### key:value pairs
my_dict = {'name':'alex'}
my_dict = {'name':'Alex', 'name':'Michael'}
my_dict
my_dict = {'name':'Alex'}
my_dict
# since python 3.7 dicts are ordered
len('Alex')
len(var)
len(tpl
)
dct = {'name':'Alex', 'age':36, 'hobby':'salsa'}
lst
lst[0]
dct[0]
dct['age']
dct
dct['hoby']
dct['hobby']
dct.get('name')
var = dct.get('name')
var\
dct.keys()
dct.values()
dct['name']
dct['name']='Alex M. Schapelle'
dct
dct.update({'age':35})
dct
# NoSql
# Excel
# ACCESS
# MSSQL
# MYSQL
# JSON
# Mongo DB
# Redis
## RDBMS: Relational Database Management System:
## SQL: Structured Query Language
## NoSQL
### MongoDB
### Redis
############################################################
# Go/Rust
dct
my_set
del my_set
del my_set2
dct
dct['eye color':'brown']
dct['eye color']='brown'
dct
dct.update({'hair color':'brown'})
dct
dct.pop('hair color')
dct
dct.popitem()
dct
clear
del dct['age']
dct
dct.clear()
dct
dct = {'name':'Alex', 'age':36, 'hobby':'salsa'}
dct['features']=['tall','ugly', 'dumb MF']
dct
dct['other_features']={'books':'comics','art':'renesans'}
dct
for i in dct:
    print(i)
for i in dct.value():
    print(i)
for i in dct.values():
    print(i)
for i in dct.keys():
    print(i)
for k,v in dct.item():
    print(k,v)
for k,v in dct.items():
    print(k,v)
dct2 = dct.copy()
dct is dct2
dct2 = dct
dct is dct2
# Data Collection:
## strings
var = '''
remember, remember, the 5th of november
'''
# V for vendetta
var
for i in var:
    print(i)
for i in var:
    print(i,end=',')
len(var)
var.split()
var.split(',')
var.split('\n')
var2 = var.split('\n')
var2
type(var2)
var.lower()
var.upper()
var.capitalize()
var.capitalize()
var.title()
var.replace('r', 'R')
var2= '''
 gun powder, treason
 '''
var3 = var + var2
var
var3
print(var3)
strng = 'hello, name is  {}, and I live in {}'
strng.format('Alex', 'Givatayim')
print(strng.format('Alex', 'Givatayim'))
lst
lst.format('alex')
print(%s,%f,%d)%('Alex',35.11,100)
print('%s,%f,%d')%('Alex',35.11,100)
print('%s,%f,%d')('Alex',35.11,100)
# print('%s,%f,%d') %('Alex',35.11,100)
help(print())
help(print)
help(print)
print('\t')
print('here\there ')
print('hello world')
print('hello\bworld')
txt = "\110\145\154\154\157"
print(txt)
