
# Welcome to Tic Tac Toe - this game is still in the making :)

"""
function for game rules
function for game board
function for getting x
function to check if theres 3 in a line


1|2|3
4|5|6
7|8|9

if the user says press one and enter whan 1 replaced with x (replace is a built-in function)
the other player press another number than the number replaces with 0 (replace is a built-in function)
we need to print the board after every turn of one of the players
we also need to set max play (when the board is full --> 9) - there's a for loop doen below

"""


# this is the main function
def game_rules():
    print("Hello. Welcome to Tic Tac Toe.")

    while True:
        agreement = input("Are you ready to play?\n Yes/No")
        if agreement == "Yes":
            tutorial()
            break
            # choose your character
        elif agreement == "No":
            print("Sorry to see you go...")
            break
        else:
            print("Sorry, now a valid answer...")



    # game tutorial function
def tutorial():
    while True:
        tutorial_answer = input("Would you like a quick tutorial? Yes/No/Exit\n")
        if tutorial_answer == "Yes":
            print("""Game is on! These are the RULES FOR TIC-TAC-TOE
        1.The game is played on a grid that's 3 squares by 3 squares.
        2. You are X, your friend (or the computer in this case) is O. Players take turns putting their marks in empty squares.
        3.The first player to get 3 of her marks in a row (up, down, across, or diagonally) is the winner.
        4.When all 9 squares are full, the game is over. If no player has 3 marks in a row, the game ends in a tie.""")

        elif tutorial_answer == "No":
            print("OK, Let's get started!")
            # start the game
            game_replay()
        elif tutorial_answer == "Exit":
            return
        else:
            print("Sorry, this is not a valid answer.")


# THE GAME BOARD FUNCTION IS STORED IN THE "game_board.py" FILE.
# The game board funtion will be placed here



# for loop in order to play the game 9 times (until the board is full) and then ask the player to replay the game
def game_replay():
    for i in range(0, 10):
        # play the game 9 times

        print("The game is over.")





