
"""


def replace_txt_chars(txt_from_outside):

    lst1 = ['o', 'O', 'l', 'L', 'E', 'e', 'T', 't', 'S', 's']
    lst2 = [0,0,1,1,3,3,7,7,5,5]
    new_txt = txt_from_outside
    for index in range(len(lst1)):
        new_txt = new_txt.replace(lst1[index]), str(lst2[index])
    return new_txt

replace_txt_chars()
"""

"""
def sum_of_sums(last_num):
    result = 0
    for num in range(last_num+1):
        result = result + num
    print(result)
sum_of_sums(5)
"""
"""
def sum_of_sums(last_num):
    result = 1
    for num in range(1, last_num+1):
        result = result * num
    print(result)
sum_of_sums(5)
"""

# This is one option:
"""
def sum_of_digits():
    i = 0
    given_number = input('Hello. Please choose your numbers right here:\n')
    num1 = given_number[i]
    num2 = given_number[i+1]
    final_num = int(num1) + int(num2)
    return final_num

print(sum_of_digits(123))
"""

# This is another option:
"""
def sum_of_digits(num):
    num_txt = str(num)
    result = 0
    for char in num_txt:
        result = result + int(char)
    return result
print(sum_of_digits(123))
"""
"""
lst = [1,2, "test", "test", 2,4,5,6,2,2,2,2,"test"]
returned = [1,2,"test", 4,5,6]


def unique_list(lst):
    result = []
    for item in len(lst):
        if item not in result:
            result.append(item)
        else:
            return result
    print("This is the results:", result)
    """