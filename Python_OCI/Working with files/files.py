# How to work with a file:
"""
f = open("oneFile.txt", "a")
variable_name = ("file_name", "mode")
f will be the placeholder of the file, like a variable.
OneFile.txt is the name of the file
"a" us the mode which we want to open our file with
"""

# Files openning modes:
"""
R = Reasd
A = Append
W = Write
X = Create
"""

# Ways to read files ( "R" mode)
"""
"t" will open the txt file as a text.
"b" will open the file in a binary mode
"""

# How to print specific lines in a txt file.
"""
print(f.readline()) --> prints the next line in the file
print(f.read(5)) --> this action will print the next 5 lines in the txt file
"""

# Example:
"""
f = open(r"c:\Users\User\test.txt", 'a')
for num in range(50):
    f.write(f"test{num}\n") # The write oprion will overwrite the file each time.
f.close()
print(f.read())
"""

# We can also open files without closing the file with "with":
"""
with open("file_name", "mode")
When we will exit the scope (the code) the file will be closed by itself automatically
To save a file as a variable --> with open("file_name", "mode") as f --> we're saving the file in a temp var callrd "f".
"""

# Plus notes
"""
f.close() --> how to close the file. This action is critical in order to prevent problems.
variable_name = open(r"c:\users\user\downloads\test.txt","r") - the "r" in the beginning of the file's location treat the string as a string only.
"""