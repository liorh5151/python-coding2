
# THIS IS EXERCISE FOR FILES HANDLING IN PYTHON


# Task - function that gets a file and read a number of lines for my choosing
"""
# This is one option (inside variable)
def file_reader():
    f = open(r"test.txt", 'x')
    f = open(r"test.txt", 'r')
    number = int(input("Hey, Please choose a number"))
    print(f.read(number))
    f.close()

# This is another options (outside variable)
def file_reader(file, number):
    f = open("file", "r")
    for i in range(number):
        print(f.readline())
    f.close()
"""


# Task - function that gets a number and creates a number of files by that same number
"""
def files_creation(numbers):
    for number in range(numbers):
        f = open("{}.txt".format(number), 'x')
        f.close()
"""

# Task - function that gets 3 strings (2 file) and creates a new file that combaines every line accordingly to a 3rd file.
"""
def files_combiner(file1, file2):
    f = open("file1", 'r')
    k = open("file2", 'r')
    n = open("file3", 'w')
    for i in file1 and file2:
        n.write(file1[i] + file2[i])
    print(n.readlines())
    f.close()
    k.close()
    n.close()
"""


